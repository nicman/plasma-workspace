# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2009, 2010, 2011, 2012, 2014, 2015, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_lockout\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-15 00:20+0000\n"
"PO-Revision-Date: 2021-09-27 21:00+0200\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Generelt"

#: contents/ui/ConfigGeneral.qml:36
#, kde-format
msgctxt ""
"Heading for a list of actions (leave, lock, switch user, hibernate, suspend)"
msgid "Show actions:"
msgstr "Vis handlinger:"

#: contents/ui/ConfigGeneral.qml:37
#, kde-format
msgid "Logout"
msgstr "Log ud"

#: contents/ui/ConfigGeneral.qml:43
#, kde-format
msgid "Shutdown"
msgstr "Luk ned"

#: contents/ui/ConfigGeneral.qml:48
#, kde-format
msgid "Reboot"
msgstr "Genstart"

#: contents/ui/ConfigGeneral.qml:53 contents/ui/data.js:5
#, kde-format
msgid "Lock"
msgstr "Lås"

#: contents/ui/ConfigGeneral.qml:58
#, kde-format
msgid "Switch User"
msgstr "Skift bruger"

#: contents/ui/ConfigGeneral.qml:63 contents/ui/data.js:46
#, kde-format
msgid "Hibernate"
msgstr "Dvale"

#: contents/ui/ConfigGeneral.qml:68 contents/ui/data.js:39
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Slumre"

#: contents/ui/data.js:6
#, kde-format
msgid "Lock the screen"
msgstr "Lås skærmen"

#: contents/ui/data.js:12
#, kde-format
msgid "Switch user"
msgstr "Skift bruger"

#: contents/ui/data.js:13
#, kde-format
msgid "Start a parallel session as a different user"
msgstr "Start en parallel session som en anden bruger"

#: contents/ui/data.js:18
#, kde-format
msgid "Shutdown…"
msgstr "Luk ned…"

#: contents/ui/data.js:19
#, kde-format
msgid "Turn off the computer"
msgstr "Sluk computeren"

#: contents/ui/data.js:25
#, kde-format
msgid "Restart…"
msgstr "Genstart…"

#: contents/ui/data.js:26
#, kde-format
msgid "Reboot the computer"
msgstr "Genstart computeren"

#: contents/ui/data.js:32
#, kde-format
msgid "Logout…"
msgstr "Log ud…"

#: contents/ui/data.js:33
#, kde-format
msgid "End the session"
msgstr "Afslut session"

#: contents/ui/data.js:40
#, kde-format
msgid "Sleep (suspend to RAM)"
msgstr "Slumre (suspendér til ram)"

#: contents/ui/data.js:47
#, kde-format
msgid "Hibernate (suspend to disk)"
msgstr "Dvale (suspendér til disk)"

#~ msgid "Do you want to suspend to disk (hibernate)?"
#~ msgstr "Vil du suspendere til disk (dvale)?"

#~ msgid "Yes"
#~ msgstr "Ja"

#~ msgid "No"
#~ msgstr "Nej"

#~ msgid "Do you want to suspend to RAM (sleep)?"
#~ msgstr "Vil du suspendere til ram (slumre)?"

#~ msgid "Leave"
#~ msgstr "Forlad"

#~ msgid "Leave..."
#~ msgstr "Forlad..."

#~ msgctxt "Heading for list of actions (leave, lock, shutdown, ...)"
#~ msgid "Actions"
#~ msgstr "Handlinger"

#~ msgid "Suspend"
#~ msgstr "Suspendér"

#~ msgid "Lock/Logout Settings"
#~ msgstr "Indstilling af Lås/log ud"

#~ msgid "Configure Lock/Logout"
#~ msgstr "Indstil lås/log ud"

#~ msgid "Please select one or more items on the list below."
#~ msgstr "Vælg et eller flere elementer på listen nedenfor."

#~ msgid "Entries"
#~ msgstr "Indgange"

#, fuzzy
#~| msgid "Show sleep button"
#~ msgid "Show \"switch user\" button"
#~ msgstr "Vis soveknap"

#~ msgid "Show logout button"
#~ msgstr "Vis log ud-knap"

#~ msgid "Show hibernate button"
#~ msgstr "Vis dvaleknap"

#~ msgid "Show sleep button"
#~ msgstr "Vis soveknap"
