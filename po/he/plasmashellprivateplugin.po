# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: plasmashellprivateplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-03 00:48+0000\n"
"PO-Revision-Date: 2017-05-16 06:57-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Zanata 3.9.6\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr ""

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr ""

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr ""

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr ""

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "מסך נעילה מאופשר"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "הגדר אם המסך ינעל אחרי זמן מוגדר"

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "זמן מוקצב לשומר מסך"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "הגדר תוך כמה דקות איזה מהמסכים ינעל"

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "הפעלה חדשה"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "מסננים"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:42
#, kde-format
msgid "Select Plasmoid File"
msgstr ""

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:66
#, kde-format
msgid "Installing the package %1 failed."
msgstr "התקנת החבילה %1 נכשלה."

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:66
#, kde-format
msgid "Installation Failure"
msgstr "ההתקנה נכשלה"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:110
#, kde-format
msgid "All Widgets"
msgstr "כל היישומונים"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
#, kde-format
msgid "Running"
msgstr "רץ"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
#, fuzzy, kde-format
#| msgid "Uninstallable"
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "לא ניתן להסרה"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
#, kde-format
msgid "Categories:"
msgstr "קטגוריות:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:199
#, kde-format
msgid "Download New Plasma Widgets"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:208
#, kde-format
msgid "Install Widget From Local File…"
msgstr ""

#~ msgid "&Execute"
#~ msgstr "ביצוע"

#~ msgid "Templates"
#~ msgstr "תבניות"

#~ msgid "Editor"
#~ msgstr "עורך"

#~ msgid "Load"
#~ msgstr "טעינה"

#~ msgid "Use"
#~ msgstr "שימוש"

#~ msgid "Output"
#~ msgstr "פלט"

#~ msgid "Open Script File"
#~ msgstr "פתח קובץ סקריפט"

#~ msgid "Save Script File"
#~ msgstr "שמור קובץ סקריפט"

#~ msgid "Executing script at %1"
#~ msgstr "מבצע את הסקריפט ב­%1"

#~ msgid "Runtime: %1ms"
#~ msgstr "זמן ריצה %1 מילישניות"
