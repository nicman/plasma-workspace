# translation of krunner_sessions.po to Japanese
# This file is distributed under the same license as the kdebase package.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2008, 2009, 2010.
# Ryuichi Yamada <ryuichi_ya220@outlook.jp>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: krunner_sessions\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-01 00:45+0000\n"
"PO-Revision-Date: 2022-08-18 14:21+0900\n"
"Last-Translator: Ryuichi Yamada <ryuichi_ya220@outlook.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 22.04.1\n"

#: sessionrunner.cpp:28 sessionrunner.cpp:64
#, kde-format
msgctxt "log out command"
msgid "logout"
msgstr "logout"

#: sessionrunner.cpp:28
#, kde-format
msgid "Logs out, exiting the current desktop session"
msgstr "現在のデスクトップセッションを終了し、ログアウトします"

#: sessionrunner.cpp:29 sessionrunner.cpp:81
#, kde-format
msgctxt "shut down computer command"
msgid "shut down"
msgstr "shut down"

#: sessionrunner.cpp:29
#, kde-format
msgid "Turns off the computer"
msgstr "コンピュータを停止します"

#: sessionrunner.cpp:33 sessionrunner.cpp:90
#, kde-format
msgctxt "lock screen command"
msgid "lock"
msgstr "lock"

#: sessionrunner.cpp:33
#, kde-format
msgid "Locks the current sessions and starts the screen saver"
msgstr "現在のセッションをロックし、スクリーンセーバーを開始します"

#: sessionrunner.cpp:36 sessionrunner.cpp:72
#, kde-format
msgctxt "restart computer command"
msgid "restart"
msgstr "restart"

#: sessionrunner.cpp:36
#, kde-format
msgid "Reboots the computer"
msgstr "コンピュータを再起動します"

#: sessionrunner.cpp:37 sessionrunner.cpp:73
#, kde-format
msgctxt "restart computer command"
msgid "reboot"
msgstr "reboot"

#: sessionrunner.cpp:40
#, kde-format
msgctxt "switch user command"
msgid "switch"
msgstr "switch"

#: sessionrunner.cpp:41
#, kde-format
msgctxt "switch user command"
msgid "switch :q:"
msgstr "switch :q:"

#: sessionrunner.cpp:42
#, kde-format
msgid ""
"Switches to the active session for the user :q:, or lists all active "
"sessions if :q: is not provided"
msgstr ""
"ユーザ :q: のアクティブセッションに切り替えます。:q: が与えられなかった場合"
"は、すべてのアクティブセッションを一覧表示します。"

#: sessionrunner.cpp:45 sessionrunner.cpp:130
#, kde-format
msgid "switch user"
msgstr "ユーザを切り替え"

#: sessionrunner.cpp:45
#, kde-format
msgid "Starts a new session as a different user"
msgstr "別のユーザで新しいセッションを開始します"

#: sessionrunner.cpp:46 sessionrunner.cpp:130
#, kde-format
msgid "new session"
msgstr "新しいセッション"

#: sessionrunner.cpp:50
#, kde-format
msgid "Lists all sessions"
msgstr "すべてのセッションを一覧表示します"

#: sessionrunner.cpp:64
#, kde-format
msgid "log out"
msgstr "ログアウト"

#: sessionrunner.cpp:66
#, kde-format
msgctxt "log out command"
msgid "Logout"
msgstr "Logout"

#: sessionrunner.cpp:75
#, kde-format
msgid "Restart the computer"
msgstr "コンピュータを再起動"

#: sessionrunner.cpp:82
#, kde-format
msgctxt "shut down computer command"
msgid "shutdown"
msgstr "shutdown"

#: sessionrunner.cpp:84
#, kde-format
msgid "Shut down the computer"
msgstr "コンピュータを停止"

#: sessionrunner.cpp:93
#, kde-format
msgid "Lock the screen"
msgstr "スクリーンをロック"

#: sessionrunner.cpp:115
#, kde-format
msgctxt "User sessions"
msgid "sessions"
msgstr "セッション"

#: sessionrunner.cpp:136
#, kde-format
msgid "Switch User"
msgstr "ユーザを切り替え"

#: sessionrunner.cpp:211
#, kde-format
msgid "Warning - New Session"
msgstr "警告 - 新しいセッション"

#: sessionrunner.cpp:212
#, kde-format
msgid ""
"<p>You have chosen to open another desktop session.<br />The current session "
"will be hidden and a new login screen will be displayed.<br />An F-key is "
"assigned to each session; F%1 is usually assigned to the first session, F%2 "
"to the second session and so on. You can switch between sessions by pressing "
"Ctrl, Alt and the appropriate F-key at the same time. Additionally, the "
"Plasma Panel and Desktop menus have actions for switching between sessions.</"
"p>"
msgstr ""
"<p>別のデスクトップセッションを開くことを選択しました。<br />現在のセッション"
"は隠されて、新たにログインスクリーンが表示されます。<br />各セッションには"
"ファンクションキーが一つずつ割り当てられます。通常、F%1 は一番最初のセッショ"
"ン、F%2 は二番目のセッション、というふうに割り当てられます。セッションを切り"
"替えるには、Ctrl と Alt と該当するファンクションキーを同時に押します。Plasma "
"パネルとデスクトップメニューからセッションを切り替えることもできます。</p>"
