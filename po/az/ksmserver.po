# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Xəyyam Qocayev <xxmn77@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-22 00:47+0000\n"
"PO-Revision-Date: 2020-07-06 23:47+0400\n"
"Last-Translator: Xəyyam Qocayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#: logout.cpp:344
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "Çıxış \"%1\" tərəfindən əngəlləndi"

#: main.cpp:117
#, kde-format
msgid "$HOME not set!"
msgstr "$HOME təyin olunmayıb"

#: main.cpp:121 main.cpp:127
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr "$HOME qovluğu (%1) mövcud deyil."

#: main.cpp:123
#, kde-format
msgid "No write access to $HOME directory (%1)."
msgstr "(%1) $HOME qovluğuna yazmaq icazəsi yoxdur."

#: main.cpp:129
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr "(%1) $HOME qovluğundan oxumaq icazəsi yoxdur."

#: main.cpp:133
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr "(%1) $HOME qovluğunda boş disk sahəsi yoxdur."

#: main.cpp:136
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr "(%2) $HOME qovluğuna yazmaq mümkün olmadı, xəta: \"%1\""

#: main.cpp:149 main.cpp:184
#, kde-format
msgid "No write access to '%1'."
msgstr "\"%1\" üçün yazmaq icazəsi yoxdur."

#: main.cpp:151 main.cpp:186
#, kde-format
msgid "No read access to '%1'."
msgstr "\"%1\" üçün oxumaq icazəsi yoxdur."

#: main.cpp:159 main.cpp:172
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr "(%1) Müvəqqəti fayllar üçün boş disk sahəsi yoxdur."

#: main.cpp:162 main.cpp:175
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""
"(%2) müvəqqəti fayllar üçün qovluğa yazmaq mümkün olmadı\n"
"    \"%1\" xətası baş verdi"

#: main.cpp:190
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""
"Plasma başladılmağa cəhd edildikdə aşağıdakı\n"
"problem aşkar edildi:"

#: main.cpp:193
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""
"\n"
"\n"
"Plasma başladıla bilmədi.\n"

#: main.cpp:200
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr "Plasma İş sahəsinin qurulmasında problem!"

#: main.cpp:236
#, kde-format
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"Standart X11R6 sessiya idarəetmə protokolunu (XSMP) dəstəkləyən \n"
"edən etibarlı Plazma sessiya meneceri."

#: main.cpp:240
#, kde-format
msgid "Restores the saved user session if available"
msgstr "Əgər mövcuddursa saxlanılan istifadəçi sesiyalarını bərpa edir"

#: main.cpp:243
#, kde-format
msgid "Also allow remote connections"
msgstr "Həmçinin məsafəli rabitəyə icazə vermək"

#: main.cpp:246
#, kde-format
msgid "Starts the session in locked mode"
msgstr "Sesiyanı kilidlənmiş rejimdə başladır"

#: main.cpp:250
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""
"Kilid ekranını dəstəklənmədən başladılır. Kilid ekranını yalnız başqa "
"komponentlər təmin etdikdə tələb olunur."

#: server.cpp:873
#, kde-format
msgid "Session Management"
msgstr "Sesiyaları idarə etmək"

#: server.cpp:876
#, kde-format
msgid "Log Out"
msgstr "Sesiyadan çıxış"

#: server.cpp:881
#, kde-format
msgid "Log Out Without Confirmation"
msgstr "Təsdiqləmədən sesiyadan çıxış"

#: server.cpp:886
#, kde-format
msgid "Halt Without Confirmation"
msgstr "Təsdiqləmədən kompyuteri söndürmək"

#: server.cpp:891
#, kde-format
msgid "Reboot Without Confirmation"
msgstr "Təsdiqləmədən kompyuteri yenidən başlatmaq"
