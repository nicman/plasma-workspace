# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Xəyyam Qocayev <xxmn77@gmail.com>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-29 00:48+0000\n"
"PO-Revision-Date: 2022-04-22 12:42+0400\n"
"Last-Translator: Kheyyam Gojayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/ui/ChangePassword.qml:26
#: package/contents/ui/UserDetailsPage.qml:166
#, kde-format
msgid "Change Password"
msgstr "Şifrəni dəyişmək"

#: package/contents/ui/ChangePassword.qml:41
#, kde-format
msgid "Password"
msgstr "Şifrə"

#: package/contents/ui/ChangePassword.qml:55
#, kde-format
msgid "Confirm password"
msgstr "Şifrəni təsdiq etmək"

#: package/contents/ui/ChangePassword.qml:68
#: package/contents/ui/CreateUser.qml:65
#, kde-format
msgid "Passwords must match"
msgstr "Şifrələr eyni olmalıdır"

#: package/contents/ui/ChangePassword.qml:73
#, kde-format
msgid "Set Password"
msgstr "Şifrə yaratmaq"

#: package/contents/ui/ChangeWalletPassword.qml:17
#, kde-format
msgid "Change Wallet Password?"
msgstr "Cüzdan Şifrəsi dəyişdirilsin?"

#: package/contents/ui/ChangeWalletPassword.qml:26
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"Sistemə giriş şifrəsini dəyişdiyiniz zaman uyğun olması üçün KWallet-in də "
"şifrəsini dəyişə bilərsiniz."

#: package/contents/ui/ChangeWalletPassword.qml:31
#, kde-format
msgid "What is KWallet?"
msgstr "KWallet nədir?"

#: package/contents/ui/ChangeWalletPassword.qml:41
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"KWallet, simsiz şəbəkələr və digər şifrələnmiş mənbələr üçün şifrələrinizi "
"saxlayan bir şifrə meneceridir. Giriş şifrənizdən fərqli olaraq bu fərdi "
"şifrənizlə kilidlənir. İki şifrə uyğun gələrsə, girişdə avtomatik olaraq "
"kiliddən çıxarıla bilər, beləliklə KWallet şifrənizi özünüz daxil etməyiniz "
"lazım olmayacaq."

#: package/contents/ui/ChangeWalletPassword.qml:57
#, kde-format
msgid "Change Wallet Password"
msgstr "Cüzdan Şifrəsini Dəyişmək"

#: package/contents/ui/ChangeWalletPassword.qml:66
#, kde-format
msgid "Leave Unchanged"
msgstr "Dəyişimi Tərk Etmək"

#: package/contents/ui/CreateUser.qml:16
#, kde-format
msgid "Create User"
msgstr "İstifadəçi yaratmaq"

#: package/contents/ui/CreateUser.qml:30
#: package/contents/ui/UserDetailsPage.qml:134
#, kde-format
msgid "Name:"
msgstr "Ad:"

#: package/contents/ui/CreateUser.qml:34
#: package/contents/ui/UserDetailsPage.qml:141
#, kde-format
msgid "Username:"
msgstr "İstifadəçi adı:"

#: package/contents/ui/CreateUser.qml:44
#: package/contents/ui/UserDetailsPage.qml:149
#, kde-format
msgid "Standard"
msgstr "Standard"

#: package/contents/ui/CreateUser.qml:45
#: package/contents/ui/UserDetailsPage.qml:150
#, kde-format
msgid "Administrator"
msgstr "Administrator"

#: package/contents/ui/CreateUser.qml:48
#: package/contents/ui/UserDetailsPage.qml:153
#, kde-format
msgid "Account type:"
msgstr "Hesab növü:"

#: package/contents/ui/CreateUser.qml:53
#, kde-format
msgid "Password:"
msgstr "Şifrə:"

#: package/contents/ui/CreateUser.qml:58
#, kde-format
msgid "Confirm password:"
msgstr "Şifrəni təsdiq etmək:"

#: package/contents/ui/CreateUser.qml:69
#, kde-format
msgid "Create"
msgstr "Yaratmaq"

#: package/contents/ui/FingerprintDialog.qml:57
#, kde-format
msgid "Configure Fingerprints"
msgstr "Barmaq izlərini tənzimləyin"

#: package/contents/ui/FingerprintDialog.qml:67
#, kde-format
msgid "Clear Fingerprints"
msgstr "Barmaq izlərini tənzimləyin"

#: package/contents/ui/FingerprintDialog.qml:74
#, kde-format
msgid "Add"
msgstr "Əlavə edin"

#: package/contents/ui/FingerprintDialog.qml:83
#: package/contents/ui/FingerprintDialog.qml:100
#, kde-format
msgid "Cancel"
msgstr "İmtina"

#: package/contents/ui/FingerprintDialog.qml:89
#, kde-format
msgid "Continue"
msgstr "Davam edin"

#: package/contents/ui/FingerprintDialog.qml:108
#, kde-format
msgid "Done"
msgstr "Hazırdır"

#: package/contents/ui/FingerprintDialog.qml:131
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "Barmaq izi qeydiyyatı"

#: package/contents/ui/FingerprintDialog.qml:137
#, kde-format
msgctxt ""
"%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgstr "Lütfən barmaq izi sensorundakı %2 üçün %1 addımını təkrarlayın."

#: package/contents/ui/FingerprintDialog.qml:147
#, kde-format
msgid "Finger Enrolled"
msgstr "Barmaq izi qeyd olundu"

#: package/contents/ui/FingerprintDialog.qml:183
#, kde-format
msgid "Pick a finger to enroll"
msgstr "Qeydə alınacaq barmağınızı seçin"

#: package/contents/ui/FingerprintDialog.qml:247
#, kde-format
msgid "Re-enroll finger"
msgstr "Təkrar qeydiyyatdan keçirin"

#: package/contents/ui/FingerprintDialog.qml:265
#, kde-format
msgid "No fingerprints added"
msgstr "Barmaq izləri əlavə olunmadı"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "Manage Users"
msgstr "İstifadəçilərin idarə edilməsi"

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Add New User"
msgstr "Yeni İstifadəçi əlavə etmək"

#: package/contents/ui/UserDetailsPage.qml:89
#, kde-format
msgid "Choose a picture"
msgstr "Şəkil seçin"

#: package/contents/ui/UserDetailsPage.qml:120
#, kde-format
msgid "Change avatar"
msgstr "Avatarı dəyişmək"

#: package/contents/ui/UserDetailsPage.qml:162
#, kde-format
msgid "Email address:"
msgstr "E-poçt ünvanı:"

#: package/contents/ui/UserDetailsPage.qml:186
#, kde-format
msgid "Delete files"
msgstr "Faylları Silmək"

#: package/contents/ui/UserDetailsPage.qml:193
#, kde-format
msgid "Keep files"
msgstr "Faylları saxlamaq"

#: package/contents/ui/UserDetailsPage.qml:200
#, kde-format
msgid "Delete User…"
msgstr "İstifadəçini silmək..."

#: package/contents/ui/UserDetailsPage.qml:211
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "Barmaq izi ilə kimlik doğrulamasını tənzimləyin..."

#: package/contents/ui/UserDetailsPage.qml:225
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"Barmaq izi ekranın kiliddən çıxarılması, tətbiqlər və əmr sətri proqramları "
"üçün şifrə tələb olunduqda həmin şifrənin əvəzinə istifadə olunur.<nl/><nl/"
">Barmaq izi istifadə etməklə sistemə daxil olmaq həhləlik dəstəklənmir."

#: package/contents/ui/UserDetailsPage.qml:235
#, kde-format
msgid "Change Avatar"
msgstr "Avatarı dəyişmək"

#: package/contents/ui/UserDetailsPage.qml:238
#, kde-format
msgid "It's Nothing"
msgstr "Heç nə"

#: package/contents/ui/UserDetailsPage.qml:239
#, kde-format
msgid "Feisty Flamingo"
msgstr "Küsəyən Flaminqo"

#: package/contents/ui/UserDetailsPage.qml:240
#, kde-format
msgid "Dragon's Fruit"
msgstr "Əjdaha meyvəsi"

#: package/contents/ui/UserDetailsPage.qml:241
#, kde-format
msgid "Sweet Potato"
msgstr "Şirin kartof"

#: package/contents/ui/UserDetailsPage.qml:242
#, kde-format
msgid "Ambient Amber"
msgstr "Kəhraba mühiti"

#: package/contents/ui/UserDetailsPage.qml:243
#, kde-format
msgid "Sparkle Sunbeam"
msgstr "Parıldayan Günəş şüası"

#: package/contents/ui/UserDetailsPage.qml:244
#, kde-format
msgid "Lemon-Lime"
msgstr "Limon-Laym"

#: package/contents/ui/UserDetailsPage.qml:245
#, kde-format
msgid "Verdant Charm"
msgstr "Cazibədar yaşıllıq"

#: package/contents/ui/UserDetailsPage.qml:246
#, kde-format
msgid "Mellow Meadow"
msgstr "Yumuşaq çəmənlik"

#: package/contents/ui/UserDetailsPage.qml:247
#, kde-format
msgid "Tepid Teal"
msgstr "İlıq dəniz mavisi"

#: package/contents/ui/UserDetailsPage.qml:248
#, kde-format
msgid "Plasma Blue"
msgstr "Mavi plasma"

#: package/contents/ui/UserDetailsPage.qml:249
#, kde-format
msgid "Pon Purple"
msgstr "Bənövşəyi Pon"

#: package/contents/ui/UserDetailsPage.qml:250
#, kde-format
msgid "Bajo Purple"
msgstr "Bənövşəyi Bajo"

#: package/contents/ui/UserDetailsPage.qml:251
#, kde-format
msgid "Burnt Charcoal"
msgstr "Yanmış kömür"

#: package/contents/ui/UserDetailsPage.qml:252
#, kde-format
msgid "Paper Perfection"
msgstr "Kağız Mükəmməlliyi"

#: package/contents/ui/UserDetailsPage.qml:253
#, kde-format
msgid "Cafétera Brown"
msgstr "Braun kafesi"

#: package/contents/ui/UserDetailsPage.qml:254
#, kde-format
msgid "Rich Hardwood"
msgstr "Rich Hardwood"

#: package/contents/ui/UserDetailsPage.qml:305
#, kde-format
msgid "Choose File…"
msgstr "Fayl seçmək..."

#: src/fingerprintmodel.cpp:147 src/fingerprintmodel.cpp:254
#, kde-format
msgid "No fingerprint device found."
msgstr "Barmaq izi cihazı tapılmadı."

#: src/fingerprintmodel.cpp:313
#, kde-format
msgid "Retry scanning your finger."
msgstr "Barmağınızı təkrar oxudun."

#: src/fingerprintmodel.cpp:315
#, kde-format
msgid "Swipe too short. Try again."
msgstr "Çox qısa sürüşdürün. Yenidən cəhd edin."

#: src/fingerprintmodel.cpp:317
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "Barmaq oxuyucunun mərkəzində deyil. Yenidən cəhd edin."

#: src/fingerprintmodel.cpp:319
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "Barmağınızı oxuyucudan qaldırın və yenidən cəhd edin."

#: src/fingerprintmodel.cpp:327
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "Barmaq izi qeydiyyatı uğursuz oldu."

#: src/fingerprintmodel.cpp:330
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr ""
"Bu cihaz üçün boş yer qalmadı, davam etmək üçün digər barmaq izlərini silin."

#: src/fingerprintmodel.cpp:333
#, kde-format
msgid "The device was disconnected."
msgstr "Bu cihazla bağlantı kəsildi."

#: src/fingerprintmodel.cpp:338
#, kde-format
msgid "An unknown error has occurred."
msgstr "Bilinməyən xəta baş verdi."

#: src/user.cpp:264
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "%1 istifadəçini saxlamaq üçün icazə alına bilmədi"

#: src/user.cpp:269
#, kde-format
msgid "There was an error while saving changes"
msgstr "Dəyişikliklərin saxlanılmasında xəta baş verdi"

#: src/user.cpp:364
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr "Şəklin ölçüsü dəyişdirilə bilmədi: müvəqqəti fayl açıla bilmədi"

#: src/user.cpp:372
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr "Şəklin ölçüsü dəyişdirilə bilmədi: müvəqqəti fayl yazıla bilmədi"

#: src/usermodel.cpp:138
#, kde-format
msgid "Your Account"
msgstr "Hesabınız"

#: src/usermodel.cpp:138
#, kde-format
msgid "Other Accounts"
msgstr "Digər hesablar"

#~ msgid "Please repeatedly "
#~ msgstr "Lütfən təkrarlayın "

#~ msgctxt "Example email address"
#~ msgid "john.doe@kde.org"
#~ msgstr "john.doe@kde.org"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Xəyyam Qocayev"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "xxmn77@gmail.com"

#~ msgid "Manage user accounts"
#~ msgstr "İstifadəçi hesablarının idarə edilməsi"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Carson Black"
#~ msgstr "Carson Black"

#~ msgid "Devin Lin"
#~ msgstr "Devin Lin"

#~ msgctxt "Example name"
#~ msgid "John Doe"
#~ msgstr "John Doe"

#~ msgid "Fingerprints"
#~ msgstr "Barmaq izləri"
