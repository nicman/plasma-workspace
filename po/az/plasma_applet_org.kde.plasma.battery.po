# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Xəyyam Qocayev <xxmn77@gmail.com>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-16 00:46+0000\n"
"PO-Revision-Date: 2022-05-26 18:43+0400\n"
"Last-Translator: Kheyyam Gojayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.1\n"

#: package/contents/ui/BatteryItem.qml:105
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:166
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""
"Bu batareyanın ömrü yalnız %1%-dir və yenisi ilə əvəz olunmalıdır. "
"Batareyanın istehsalçısı ilə əlaqə yaradın."

#: package/contents/ui/BatteryItem.qml:181
#, kde-format
msgid "Time To Full:"
msgstr "Dolma müddəti:"

#: package/contents/ui/BatteryItem.qml:182
#, kde-format
msgid "Remaining Time:"
msgstr "Qalan vaxt:"

#: package/contents/ui/BatteryItem.qml:198
#, kde-format
msgid "Battery Health:"
msgstr "Batareyanın vəziyyəti:"

#: package/contents/ui/BatteryItem.qml:204
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:218
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr "Batareya təqribən %1%-ə qədər doldurmaq üçün ayarlanmışdır."

#: package/contents/ui/BrightnessItem.qml:64
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/CompactRepresentation.qml:84
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/logic.js:12
#, kde-format
msgid "Discharging"
msgstr "Boşalır"

#: package/contents/ui/logic.js:13 package/contents/ui/main.qml:136
#, kde-format
msgid "Fully Charged"
msgstr "Tam Dolmuş"

#: package/contents/ui/logic.js:14
#, kde-format
msgid "Charging"
msgstr "Dolur"

#: package/contents/ui/logic.js:16
#, kde-format
msgid "Not Charging"
msgstr "Dolmur"

#: package/contents/ui/logic.js:19
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "Mövcud deyil"

#: package/contents/ui/main.qml:99 package/contents/ui/main.qml:327
#, kde-format
msgid "Battery and Brightness"
msgstr "Batareya və Parlaqlıq"

#: package/contents/ui/main.qml:100
#, kde-format
msgid "Brightness"
msgstr "Parlaqlıq"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Battery"
msgstr "Batareya"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Power Management"
msgstr "Enerjiyə nəzarət"

#: package/contents/ui/main.qml:143
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr "Batareya %1%-dir, Qidalanma getmir"

#: package/contents/ui/main.qml:145
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr "Batareya %1%-dir, qoşuludur, lakin hələ də boşalma gedir"

#: package/contents/ui/main.qml:147
#, kde-format
msgid "Battery at %1%, Charging"
msgstr "Batareya %1%-dir, Doldurulur"

#: package/contents/ui/main.qml:150
#, kde-format
msgid "Battery at %1%"
msgstr "Batareya %1%-dir"

#: package/contents/ui/main.qml:158
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr "Qida bloku batareyanı doldutacaq qədər güclü deyil"

#: package/contents/ui/main.qml:162
#, kde-format
msgid "No Batteries Available"
msgstr "Əlçatan batareyalar yoxdur"

#: package/contents/ui/main.qml:168
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr "Tam dolana qədər %1"

#: package/contents/ui/main.qml:170
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr "%1 qaldı"

#: package/contents/ui/main.qml:173
#, kde-format
msgid "Not charging"
msgstr "Qidalanmır"

#: package/contents/ui/main.qml:177
#, kde-format
msgid "Automatic sleep and screen locking are disabled"
msgstr "Avtomatik yuxu və ekran kilidlənməsi qeyri-aktivdir"

#: package/contents/ui/main.qml:276
#, kde-format
msgid "The battery applet has enabled system-wide inhibition"
msgstr "Batareya tətbiqi ümumi sistem əngəllənməsini aktiv etdi"

#: package/contents/ui/main.qml:330
#, kde-format
msgid "Failed to activate %1 mode"
msgstr "%1 rejimi aktiv eddilə bilmədi"

#: package/contents/ui/main.qml:342
#, kde-format
msgid "&Show Energy Information…"
msgstr "&Enerji məlumatlarını göstərmək..."

#: package/contents/ui/main.qml:344
#, kde-format
msgid "Show Battery Percentage on Icon"
msgstr "Batareyanın faizi nişanda göstərilsin"

#: package/contents/ui/main.qml:350
#, kde-format
msgid "&Configure Energy Saving…"
msgstr "&Enerjiyə qənaət rejimini tənzimləyin..."

#: package/contents/ui/PopupDialog.qml:117
#, kde-format
msgid "Display Brightness"
msgstr "Ekran parlaqlığı"

#: package/contents/ui/PopupDialog.qml:146
#, kde-format
msgid "Keyboard Brightness"
msgstr "Klaviatura işıqlanması"

#: package/contents/ui/PowerManagementItem.qml:37
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid "Manually block sleep and screen locking"
msgstr "Avtomatik yuxu və ekran kilidinə keçidi əl ilə əngəlləmək"

#: package/contents/ui/PowerManagementItem.qml:70
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""
"Sizin noutbukunuz xarici monitor qoşulduqda qapağı bağlanarsa yuxu rejiminə "
"keçməmək üçün tənzimlənmişdir."

#: package/contents/ui/PowerManagementItem.qml:83
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] ""
"%1 tətbiqi hazırda yuxu rejimini və ekranın kilidlənməsini əngəlləyir:"
msgstr[1] "%1 tətbiq hazırda yuxu və ekran kilidini əngəlləyir:"

#: package/contents/ui/PowerManagementItem.qml:103
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr "%1 hazırda yuxu rejimini və ekranın kilidlənməsini əngəlləyir (%2)"

#: package/contents/ui/PowerManagementItem.qml:104
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr ""
"%1 hazırda yuxu rejimini və ekranın kilidlənməsini əngəlləyir (səbəbi "
"bilinmir)"

#: package/contents/ui/PowerManagementItem.qml:106
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerManagementItem.qml:107
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr "%1: naməlum səbəb"

#: package/contents/ui/PowerProfileItem.qml:34
#, kde-format
msgid "Power Save"
msgstr "Enerjiyə qənaət"

#: package/contents/ui/PowerProfileItem.qml:38
#, kde-format
msgid "Balanced"
msgstr "Tarazlaşdırılmış"

#: package/contents/ui/PowerProfileItem.qml:42
#, kde-format
msgid "Performance"
msgstr "Məhsuldar"

#: package/contents/ui/PowerProfileItem.qml:59
#, kde-format
msgid "Power Profile"
msgstr "Enerji profili"

#: package/contents/ui/PowerProfileItem.qml:191
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""
"Kompyuterin məhsuldar işi zəifləyə bilər çünki kompyuter elektrik şəbəkəsinə "
"qoşulu olmadığını aşkarladı."

#: package/contents/ui/PowerProfileItem.qml:193
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr ""
"Məhsuldar rejim əlçatan deyil, çünki qızma hərarətinin həddan artıq olduğu "
"aşkarlandı."

#: package/contents/ui/PowerProfileItem.qml:195
#, kde-format
msgid "Performance mode is unavailable."
msgstr "Məhsuldar rejim əlçatan deyil."

#: package/contents/ui/PowerProfileItem.qml:208
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""
"Kompyuterin məhsuldar işi zəifləyə bilər çünki kompyuter elektrik şəbəkəsinə "
"qoşulu olmadığını aşkarladı."

#: package/contents/ui/PowerProfileItem.qml:210
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr ""
"Kompyuterin məhsuldar işi zəifləyə bolər çünki qızma hərarətinin həddan "
"artıq olduğu aşkarlandı."

#: package/contents/ui/PowerProfileItem.qml:212
#, kde-format
msgid "Performance may be reduced."
msgstr "Məhsuldar işləməsi azala bilər."

#: package/contents/ui/PowerProfileItem.qml:223
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] "Bir tətbiq %2 işə salınmaını tələb edir:"
msgstr[1] "%1 tətbiq %2 işə salınmasını tələb edir:"

#: package/contents/ui/PowerProfileItem.qml:241
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr "%1: %2"

#~ msgid ""
#~ "Performance mode is unavailable because the computer has detected it is "
#~ "sitting on your lap."
#~ msgstr ""
#~ "Məhsuldar rejim əlçatan deyil, çünki kompyuter elektrik şəbəkəsinə qoşulu "
#~ "olmadığını aşkarladı."

#~ msgid "General"
#~ msgstr "Əsas"

#~ msgid "An application is preventing sleep and screen locking:"
#~ msgstr "Tətbiq yuxu rejiminə və ekran kilidlənməsinin qarşısını alır:"

#~ msgctxt "short symbol to signal there is no battery currently available"
#~ msgid "-"
#~ msgstr "-"

#~ msgid "Configure Power Saving..."
#~ msgstr "Enerjiyə qənaət rejimini tənzimləmək..."

#~ msgid "Time To Empty:"
#~ msgstr "Boşalma müddəti:"
