# translation of kcm_autostart.po to Belarusian Latin
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Ihar Hrachyshka <ihar.hrachyshka@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-02 00:50+0000\n"
"PO-Revision-Date: 2008-11-04 12:19+0200\n"
"Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>\n"
"Language-Team: Belarusian Latin <i18n@mova.org>\n"
"Language: be@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || n%10>=5 && n%10<=9 || n"
"%100>=11 && n%100<=14 ? 2 : 3);\n"

#: autostartmodel.cpp:288
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr ""

#: autostartmodel.cpp:291
#, kde-format
msgid "\"%1\" does not exist."
msgstr ""

#: autostartmodel.cpp:294
#, kde-format
msgid "\"%1\" is not a file."
msgstr ""

#: autostartmodel.cpp:297
#, kde-format
msgid "\"%1\" is not readable."
msgstr ""

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Make Executable"
msgstr ""

#: package/contents/ui/main.qml:54
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: package/contents/ui/main.qml:56
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: package/contents/ui/main.qml:89
#, fuzzy, kde-format
#| msgid "&Properties"
msgid "Properties"
msgstr "&Ułaścivaści"

#: package/contents/ui/main.qml:95
#, fuzzy, kde-format
#| msgid "&Remove"
msgid "Remove"
msgstr "&Vydali"

#: package/contents/ui/main.qml:106
#, kde-format
msgid "Applications"
msgstr ""

#: package/contents/ui/main.qml:109
#, kde-format
msgid "Login Scripts"
msgstr ""

#: package/contents/ui/main.qml:112
#, fuzzy, kde-format
#| msgid "Pre-KDE startup"
msgid "Pre-startup Scripts"
msgstr "Uklučeńnie pierad „KDE”"

#: package/contents/ui/main.qml:115
#, kde-format
msgid "Logout Scripts"
msgstr ""

#: package/contents/ui/main.qml:124
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: package/contents/ui/main.qml:125
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr ""

#: package/contents/ui/main.qml:139
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Choose Login Script"
msgstr "Dadaj skrypt..."

#: package/contents/ui/main.qml:160
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: package/contents/ui/main.qml:178
#, kde-format
msgid "Add…"
msgstr ""

#: package/contents/ui/main.qml:193
#, kde-format
msgid "Add Application…"
msgstr ""

#: package/contents/ui/main.qml:199
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Login Script…"
msgstr "Dadaj skrypt..."

#: package/contents/ui/main.qml:205
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Logout Script…"
msgstr "Dadaj skrypt..."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Ihar Hračyška"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "ihar.hrachyshka@gmail.com"

#, fuzzy
#~| msgid "KDE Autostart Manager Control Panel Module"
#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "Modul kiravańnia kiraŭnikom aŭtamatyčnaha ŭklučeńnia dla „KDE”"

#, fuzzy
#~| msgid "(c) 2006-2007-2008 Autostart Manager team"
#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "© 2006, 2007, 2008 raspracoŭniki kiraŭnika aŭtamatyčnaha ŭklučeńnia"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "Dahladnik"

#, fuzzy
#~| msgid "Advanced"
#~ msgid "Add..."
#~ msgstr "Asablivaje"

#, fuzzy
#~| msgid "Shell script:"
#~ msgid "Shell script path:"
#~ msgstr "Skrypt abałonki:"

#~ msgid "Create as symlink"
#~ msgstr "Stvary symbalny łuč"

#, fuzzy
#~| msgid "Autostart only in KDE"
#~ msgid "Autostart only in Plasma"
#~ msgstr "Aŭtamatyčna ŭklučaj tolki ŭ „KDE”"

#~ msgid "Name"
#~ msgstr "Nazva"

#~ msgid "Command"
#~ msgstr "Zahad"

#~ msgid "Status"
#~ msgstr "Stan"

#, fuzzy
#~| msgctxt ""
#~| "@title:column The name of the column that decides if the program is run "
#~| "on kde startup, on kde shutdown, etc"
#~| msgid "Run On"
#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Čas uklučeńnia"

#, fuzzy
#~| msgid "KDE Autostart Manager"
#~ msgid "Session Autostart Manager"
#~ msgstr "Kiraŭnik aŭtamatyčnaha ŭklučeńnia dla „KDE”"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "Uklučanaja"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "Vyklučanaja"

#~ msgid "Desktop File"
#~ msgstr "Fajł stała"

#~ msgid "Script File"
#~ msgstr "Fajł skrypta"

#~ msgid "Add Program..."
#~ msgstr "Dadaj prahramu..."

#~ msgid "Startup"
#~ msgstr "Uklučeńnie kamputara"

#, fuzzy
#~| msgid ""
#~| "KDE only reads files with sh extensions for setting up the environment."
#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr ""
#~ "Systema „KDE” dla naładžvańnia rabočaha asiarodździa čytaje tolki fajły z "
#~ "pašyreńniem „.sh”."

#~ msgid "Shutdown"
#~ msgstr "Vyklučeńnie kamputara"

#, fuzzy
#~ msgid "1"
#~ msgstr "1"
