# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-29 00:48+0000\n"
"PO-Revision-Date: 2022-04-19 13:44+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/ui/ChangePassword.qml:26
#: package/contents/ui/UserDetailsPage.qml:166
#, kde-format
msgid "Change Password"
msgstr "Wachtwoord wijzigen"

#: package/contents/ui/ChangePassword.qml:41
#, kde-format
msgid "Password"
msgstr "Wachtwoord"

#: package/contents/ui/ChangePassword.qml:55
#, kde-format
msgid "Confirm password"
msgstr "Wachtwoord bevestigen"

#: package/contents/ui/ChangePassword.qml:68
#: package/contents/ui/CreateUser.qml:65
#, kde-format
msgid "Passwords must match"
msgstr "Wachtwoorden moeten overeenkomen"

#: package/contents/ui/ChangePassword.qml:73
#, kde-format
msgid "Set Password"
msgstr "Wachtwoord instellen"

#: package/contents/ui/ChangeWalletPassword.qml:17
#, kde-format
msgid "Change Wallet Password?"
msgstr "Wachtwoord van portefeuille wijzigen?"

#: package/contents/ui/ChangeWalletPassword.qml:26
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"Nu u uw aanmeldwachtwoord hebt gewijzigd, wilt u misschien ook het "
"wachtwoord van uw standaard KWallet wijzigen om het overeen te laten komen."

#: package/contents/ui/ChangeWalletPassword.qml:31
#, kde-format
msgid "What is KWallet?"
msgstr "Wat is KWallet?"

#: package/contents/ui/ChangeWalletPassword.qml:41
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"KWallet is een wachtwoordbeheerder die uw wachtwoorden voor draadloze "
"netwerken en andere versleutelde hulpbronnen opslaat. Het wordt vergrendeld "
"met zijn eigen wachtwoord wat verschilt van uw aanmeldwachtwoord. Als de "
"twee wachtwoorden gelijk zijn, kan het automatisch ontgrendeld worden bij "
"aanmelden zodat u het wachtwoord van KWallet niet zelf hoeft in te voeren."

#: package/contents/ui/ChangeWalletPassword.qml:57
#, kde-format
msgid "Change Wallet Password"
msgstr "Wachtwoord van portefeuille wijzigen"

#: package/contents/ui/ChangeWalletPassword.qml:66
#, kde-format
msgid "Leave Unchanged"
msgstr "Ongewijzigd laten"

#: package/contents/ui/CreateUser.qml:16
#, kde-format
msgid "Create User"
msgstr "Gebruiker aanmaken"

#: package/contents/ui/CreateUser.qml:30
#: package/contents/ui/UserDetailsPage.qml:134
#, kde-format
msgid "Name:"
msgstr "Naam:"

#: package/contents/ui/CreateUser.qml:34
#: package/contents/ui/UserDetailsPage.qml:141
#, kde-format
msgid "Username:"
msgstr "Gebruikersnaam:"

#: package/contents/ui/CreateUser.qml:44
#: package/contents/ui/UserDetailsPage.qml:149
#, kde-format
msgid "Standard"
msgstr "Standaard"

#: package/contents/ui/CreateUser.qml:45
#: package/contents/ui/UserDetailsPage.qml:150
#, kde-format
msgid "Administrator"
msgstr "Systeembeheerder"

#: package/contents/ui/CreateUser.qml:48
#: package/contents/ui/UserDetailsPage.qml:153
#, kde-format
msgid "Account type:"
msgstr "Accounttype:"

#: package/contents/ui/CreateUser.qml:53
#, kde-format
msgid "Password:"
msgstr "Wachtwoord:"

#: package/contents/ui/CreateUser.qml:58
#, kde-format
msgid "Confirm password:"
msgstr "Wachtwoord bevestigen:"

#: package/contents/ui/CreateUser.qml:69
#, kde-format
msgid "Create"
msgstr "Aanmaken"

#: package/contents/ui/FingerprintDialog.qml:57
#, kde-format
msgid "Configure Fingerprints"
msgstr "Vingerafdrukken configureren"

#: package/contents/ui/FingerprintDialog.qml:67
#, kde-format
msgid "Clear Fingerprints"
msgstr "Vingerafdrukken wissen"

#: package/contents/ui/FingerprintDialog.qml:74
#, kde-format
msgid "Add"
msgstr "Toevoegen"

#: package/contents/ui/FingerprintDialog.qml:83
#: package/contents/ui/FingerprintDialog.qml:100
#, kde-format
msgid "Cancel"
msgstr "Annuleren"

#: package/contents/ui/FingerprintDialog.qml:89
#, kde-format
msgid "Continue"
msgstr "Doorgaan"

#: package/contents/ui/FingerprintDialog.qml:108
#, kde-format
msgid "Done"
msgstr "Gereed"

#: package/contents/ui/FingerprintDialog.qml:131
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "Vingerafdruk invoeren"

#: package/contents/ui/FingerprintDialog.qml:137
#, kde-format
msgctxt ""
"%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgstr "Herhaaldelijk uw %2 %1 op de vingerprintsensor."

#: package/contents/ui/FingerprintDialog.qml:147
#, kde-format
msgid "Finger Enrolled"
msgstr "Vinger ingevoerd"

#: package/contents/ui/FingerprintDialog.qml:183
#, kde-format
msgid "Pick a finger to enroll"
msgstr "Een vinger kiezen om in te voeren"

#: package/contents/ui/FingerprintDialog.qml:247
#, kde-format
msgid "Re-enroll finger"
msgstr "Vinger opnieuw invoeren"

#: package/contents/ui/FingerprintDialog.qml:265
#, kde-format
msgid "No fingerprints added"
msgstr "Geen vingerafdruk toegevoegd"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "Manage Users"
msgstr "Gebruikers beheren"

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Add New User"
msgstr "Nieuwe gebruiker toevoegen"

#: package/contents/ui/UserDetailsPage.qml:89
#, kde-format
msgid "Choose a picture"
msgstr "Een afbeelding kiezen"

#: package/contents/ui/UserDetailsPage.qml:120
#, kde-format
msgid "Change avatar"
msgstr "Avatar vervangen"

#: package/contents/ui/UserDetailsPage.qml:162
#, kde-format
msgid "Email address:"
msgstr "E-mailadres:"

#: package/contents/ui/UserDetailsPage.qml:186
#, kde-format
msgid "Delete files"
msgstr "Bestanden verwijderen"

#: package/contents/ui/UserDetailsPage.qml:193
#, kde-format
msgid "Keep files"
msgstr "Bestanden bewaren"

#: package/contents/ui/UserDetailsPage.qml:200
#, kde-format
msgid "Delete User…"
msgstr "Gebruiker verwijderen…"

#: package/contents/ui/UserDetailsPage.qml:211
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "Authenticatie met vingerafdruk configureren"

#: package/contents/ui/UserDetailsPage.qml:225
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"Vingerafdrukken kunnen gebruikt worden in plaats van een wachtwoord bij "
"ontgrendelen van het scherm en het leveren van beheersrechten aan "
"toepassingen en opdrachtregelprogramma's die er om vragen.<nl/><nl/"
">Aanmelden in het systeem met uw vingerafdruk wordt nog niet ondersteund."

#: package/contents/ui/UserDetailsPage.qml:235
#, kde-format
msgid "Change Avatar"
msgstr "Avatar vervangen"

#: package/contents/ui/UserDetailsPage.qml:238
#, kde-format
msgid "It's Nothing"
msgstr "Het is niets"

#: package/contents/ui/UserDetailsPage.qml:239
#, kde-format
msgid "Feisty Flamingo"
msgstr "Feisty Flamingo"

#: package/contents/ui/UserDetailsPage.qml:240
#, kde-format
msgid "Dragon's Fruit"
msgstr "Dragon's Fruit"

#: package/contents/ui/UserDetailsPage.qml:241
#, kde-format
msgid "Sweet Potato"
msgstr "Sweet Potato"

#: package/contents/ui/UserDetailsPage.qml:242
#, kde-format
msgid "Ambient Amber"
msgstr "Ambient Amber"

#: package/contents/ui/UserDetailsPage.qml:243
#, kde-format
msgid "Sparkle Sunbeam"
msgstr "Sparkle Sunbeam"

#: package/contents/ui/UserDetailsPage.qml:244
#, kde-format
msgid "Lemon-Lime"
msgstr "Lemon-Lime"

#: package/contents/ui/UserDetailsPage.qml:245
#, kde-format
msgid "Verdant Charm"
msgstr "Verdant Charm"

#: package/contents/ui/UserDetailsPage.qml:246
#, kde-format
msgid "Mellow Meadow"
msgstr "Mellow Meadow"

#: package/contents/ui/UserDetailsPage.qml:247
#, kde-format
msgid "Tepid Teal"
msgstr "Tepid Teal"

#: package/contents/ui/UserDetailsPage.qml:248
#, kde-format
msgid "Plasma Blue"
msgstr "Plasma Blue"

#: package/contents/ui/UserDetailsPage.qml:249
#, kde-format
msgid "Pon Purple"
msgstr "Pon Purple"

#: package/contents/ui/UserDetailsPage.qml:250
#, kde-format
msgid "Bajo Purple"
msgstr "Bajo Purple"

#: package/contents/ui/UserDetailsPage.qml:251
#, kde-format
msgid "Burnt Charcoal"
msgstr "Burnt Charcoal"

#: package/contents/ui/UserDetailsPage.qml:252
#, kde-format
msgid "Paper Perfection"
msgstr "Paper Perfection"

#: package/contents/ui/UserDetailsPage.qml:253
#, kde-format
msgid "Cafétera Brown"
msgstr "Cafétera Brown"

#: package/contents/ui/UserDetailsPage.qml:254
#, kde-format
msgid "Rich Hardwood"
msgstr "Rich Hardwood"

#: package/contents/ui/UserDetailsPage.qml:305
#, kde-format
msgid "Choose File…"
msgstr "Bestand kiezen…"

#: src/fingerprintmodel.cpp:147 src/fingerprintmodel.cpp:254
#, kde-format
msgid "No fingerprint device found."
msgstr "Geen vingerafdrukapparaat gevonden."

#: src/fingerprintmodel.cpp:313
#, kde-format
msgid "Retry scanning your finger."
msgstr "Uw vinger opnieuw proberen te scannen."

#: src/fingerprintmodel.cpp:315
#, kde-format
msgid "Swipe too short. Try again."
msgstr "Veeg te kort. Probeer opnieuw."

#: src/fingerprintmodel.cpp:317
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "Vinger niet gecentreerd op de lezer. Opnieuw proberen."

#: src/fingerprintmodel.cpp:319
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "Uw vinger verwijderen van de lezer en opnieuw proberen."

#: src/fingerprintmodel.cpp:327
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "Invoeren van vingerafdruk is mislukt."

#: src/fingerprintmodel.cpp:330
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr ""
"Er is geen ruimte over op dit apparaat, verwijder andere vingerafdrukken om "
"door te gaan."

#: src/fingerprintmodel.cpp:333
#, kde-format
msgid "The device was disconnected."
msgstr "De verbinding met het apparaat verbroken."

#: src/fingerprintmodel.cpp:338
#, kde-format
msgid "An unknown error has occurred."
msgstr "Er is een onbekende fout opgetreden."

#: src/user.cpp:264
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "Kon geen toegangsrechten verkrijgen om gebruiker %1 op te slaan"

#: src/user.cpp:269
#, kde-format
msgid "There was an error while saving changes"
msgstr "Er was een fout tijdens opslaan van wijzigingen"

#: src/user.cpp:364
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr ""
"Afbeelding van grootte wijzigen is mislukt: openen van tijdelijk bestand is "
"mislukt "

#: src/user.cpp:372
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr ""
"Afbeelding van grootte wijzigen is mislukt: schrijven naar tijdelijk bestand "
"is mislukt "

#: src/usermodel.cpp:138
#, kde-format
msgid "Your Account"
msgstr "Uw account"

#: src/usermodel.cpp:138
#, kde-format
msgid "Other Accounts"
msgstr "Andere accounts"

#~ msgid "Please repeatedly "
#~ msgstr "Nog eens graag "

#~ msgctxt "Example email address"
#~ msgid "john.doe@kde.org"
#~ msgstr "john.doe@kde.org"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Freek de Kruijf - 2020 t/m 2021"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "freekdekruijf@kde.nl"

#~ msgid "Manage user accounts"
#~ msgstr "Uw accounts beheren"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Carson Black"
#~ msgstr "Carson Black"

#~ msgid "Devin Lin"
#~ msgstr "Devin Lin"

#~ msgctxt "Example name"
#~ msgid "John Doe"
#~ msgstr "John Doe"

#~ msgid "Fingerprints"
#~ msgstr "Vingerafdrukken"
