# translation of plasma_applet_devicenotifier.po to galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# mvillarino <mvillarino@users.sourceforge.net>, 2008, 2009.
# marce villarino <mvillarino@users.sourceforge.net>, 2009.
# Marce Villarino <mvillarino@kde-espana.es>, 2009, 2011.
# Marce Villarino <mvillarino@kde-espana.es>, 2011, 2012.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-21 00:47+0000\n"
"PO-Revision-Date: 2019-10-19 21:50+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: package/contents/ui/DeviceItem.qml:180
#, fuzzy, kde-format
#| msgctxt "@info:status Free disk space"
#| msgid "%1 free"
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr "%1 libre"

#: package/contents/ui/DeviceItem.qml:184
#, fuzzy, kde-format
#| msgctxt ""
#| "Accessing is a less technical word for Mounting; translation should be "
#| "short and mean 'Currently mounting this device'"
#| msgid "Accessing..."
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr "Estase a acceder…"

#: package/contents/ui/DeviceItem.qml:187
#, fuzzy, kde-format
#| msgctxt ""
#| "Removing is a less technical word for Unmounting; translation should be "
#| "short and mean 'Currently unmounting this device'"
#| msgid "Removing..."
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr "Retirando…"

#: package/contents/ui/DeviceItem.qml:190
#, kde-format
msgid "Don't unplug yet! Files are still being transferred..."
msgstr ""

#: package/contents/ui/DeviceItem.qml:221
#, kde-format
msgid "Open in File Manager"
msgstr ""

#: package/contents/ui/DeviceItem.qml:224
#, kde-format
msgid "Mount and Open"
msgstr ""

#: package/contents/ui/DeviceItem.qml:226
#, kde-format
msgid "Eject"
msgstr ""

#: package/contents/ui/DeviceItem.qml:228
#, kde-format
msgid "Safely remove"
msgstr ""

#: package/contents/ui/DeviceItem.qml:270
#, kde-format
msgid "Mount"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:41
#: package/contents/ui/main.qml:238
#, fuzzy, kde-format
#| msgid "Remove all"
msgid "Remove All"
msgstr "Retiralo todo"

#: package/contents/ui/FullRepresentation.qml:42
#, kde-format
msgid "Click to safely remove all devices"
msgstr "Prema para retirar con seguridade todos os dispositivos"

#: package/contents/ui/FullRepresentation.qml:186
#, fuzzy, kde-format
#| msgid "Non-removable devices only"
msgid "No removable devices attached"
msgstr "Só os dispositivos non extraíbeis"

#: package/contents/ui/FullRepresentation.qml:186
#, fuzzy, kde-format
msgid "No disks available"
msgstr "Non hai ningún dispositivo dispoñíbel"

#: package/contents/ui/main.qml:47
#, kde-format
msgid "Most Recent Device"
msgstr "O dispositivo máis recente"

#: package/contents/ui/main.qml:47
#, kde-format
msgid "No Devices Available"
msgstr "Non hai ningún dispositivo dispoñíbel"

#: package/contents/ui/main.qml:245
#, fuzzy, kde-format
#| msgid "Removable devices only"
msgid "Removable Devices"
msgstr "Só os dispositivos extraíbeis"

#: package/contents/ui/main.qml:251
#, fuzzy, kde-format
#| msgid "Non-removable devices only"
msgid "Non Removable Devices"
msgstr "Só os dispositivos non extraíbeis"

#: package/contents/ui/main.qml:257
#, fuzzy, kde-format
#| msgid "All devices"
msgid "All Devices"
msgstr "Todos os dispositivos"

#: package/contents/ui/main.qml:265
#, fuzzy, kde-format
#| msgid "Open popup when new device is plugged in"
msgid "Show popup when new device is plugged in"
msgstr "Abrir unha xanela emerxente ao conectar dispositivos novos."

#: package/contents/ui/main.qml:274
#, fuzzy, kde-format
#| msgctxt "Open auto mounter kcm"
#| msgid "Configure Removable Devices"
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "Configurar os dispositivos extraíbeis"

#~ msgid "Show:"
#~ msgstr "Mostrar:"

#~ msgid "General"
#~ msgstr "Xeral"

#~ msgid ""
#~ "It is currently <b>not safe</b> to remove this device: applications may "
#~ "be accessing it. Click the eject button to safely remove this device."
#~ msgstr ""
#~ "Na actualidade <b>non é seguro</b> retirar este dispositivo: pode haber "
#~ "aplicacións accedendo a el. Prema o botón de expulsar para retirar con "
#~ "seguranza este dispositivo."

#~ msgid "This device is currently accessible."
#~ msgstr "Este dispositivo está accesíbel."

#~ msgid ""
#~ "It is currently <b>not safe</b> to remove this device: applications may "
#~ "be accessing other volumes on this device. Click the eject button on "
#~ "these other volumes to safely remove this device."
#~ msgstr ""
#~ "Na actualidade <b>non é seguro</b> retirar este dispositivo: pode haber "
#~ "aplicacións accedendo a outros volumes do dispositivo. Prema o botón de "
#~ "expulsar deses outros volumes para retirar con seguranza este dispositivo."

#~ msgid "It is currently safe to remove this device."
#~ msgstr "Agora é seguro retirar este dispositivo."

#~ msgid "This device is not currently accessible."
#~ msgstr "Este dispositivo non está accesíbel."

#~ msgid "1 action for this device"
#~ msgid_plural "%1 actions for this device"
#~ msgstr[0] "1 acción para este dispositivo"
#~ msgstr[1] "%1 accións para este dispositivo"

#~ msgid "Click to access this device from other applications."
#~ msgstr "Prema para acceder a este dispositivo desde outras aplicacións."

#~ msgid "Click to eject this disc."
#~ msgstr "Prema para expulsar este disco."

#~ msgid "Click to safely remove this device."
#~ msgstr "Prema para retirar con seguridade este dispositivo."

#~ msgid "Click to mount this device."
#~ msgstr "Prema para montar este dispositivo."

#~ msgid "Available Devices"
#~ msgstr "Dispositivos dispoñíbel"
