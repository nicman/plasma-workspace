# Translation of plasma_engine_mpris2.po to Catalan
# Copyright (C) 2012-2021 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2012, 2015, 2018, 2020, 2021.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-15 00:20+0000\n"
"PO-Revision-Date: 2021-04-21 13:26+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#: multiplexedservice.cpp:68
#, kde-format
msgctxt "Name for global shortcuts category"
msgid "Media Controller"
msgstr "Controlador multimèdia"

#: multiplexedservice.cpp:70
#, kde-format
msgid "Play/Pause media playback"
msgstr "Reprodueix/Fa pausa del reproductor de suports"

#: multiplexedservice.cpp:88
#, kde-format
msgid "Media playback next"
msgstr "Següent del reproductor de suports"

#: multiplexedservice.cpp:97
#, kde-format
msgid "Media playback previous"
msgstr "Anterior del reproductor de suports"

#: multiplexedservice.cpp:106
#, kde-format
msgid "Stop media playback"
msgstr "Atura el reproductor de suports"

#: multiplexedservice.cpp:115
#, kde-format
msgid "Pause media playback"
msgstr "Fes pausa al reproductor de suports"

#: multiplexedservice.cpp:124
#, kde-format
msgid "Play media playback"
msgstr "Reprodueix el reproductor de suports"

#: multiplexedservice.cpp:133
#, kde-format
msgid "Media volume up"
msgstr "Apuja el volum del suport"

#: multiplexedservice.cpp:142
#, kde-format
msgid "Media volume down"
msgstr "Abaixa el volum del suport"

#: playeractionjob.cpp:169
#, kde-format
msgid "The media player '%1' cannot perform the action '%2'."
msgstr "El reproductor de suports «%1» no ha pogut realitzar l'acció «%2»."

#: playeractionjob.cpp:171
#, kde-format
msgid "Attempting to perform the action '%1' failed with the message '%2'."
msgstr "Ha fallat en intentar executar l'acció «%1» amb el missatge «%2»."

#: playeractionjob.cpp:173
#, kde-format
msgid "The argument '%1' for the action '%2' is missing or of the wrong type."
msgstr ""
"L'argument «%1» per a l'acció «%2» no hi és o és d'un tipus incorrecte."

#: playeractionjob.cpp:175
#, kde-format
msgid "The operation '%1' is unknown."
msgstr "L'operació «%1» és desconeguda."

#: playeractionjob.cpp:177
#, kde-format
msgid "Unknown error."
msgstr "Error desconegut."
