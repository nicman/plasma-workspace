# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Luigi Toscano <luigi.toscano@tiscali.it>, 2015, 2016, 2018, 2020, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-03 00:48+0000\n"
"PO-Revision-Date: 2022-05-24 21:17+0200\n"
"Last-Translator: Luigi Toscano <luigi.toscano@tiscali.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Luigi Toscano"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "luigi.toscano@tiscali.it"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Festività"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Eventi"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Da fare"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Altro"

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Blocco dello schermo abilitato"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Imposta se lo schermo debba essere bloccato dopo il tempo indicato."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Scadenza per il salvaschermo"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Imposta dopo quanti minuti lo schermo viene bloccato."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Nuova sessione"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Filtri"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:42
#, kde-format
msgid "Select Plasmoid File"
msgstr "Seleziona un file di plasmoide"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:66
#, kde-format
msgid "Installing the package %1 failed."
msgstr "Installazione del pacchetto %1 non riuscita."

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:66
#, kde-format
msgid "Installation Failure"
msgstr "Installazione non riuscita"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:110
#, kde-format
msgid "All Widgets"
msgstr "Tutti gli oggetti"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
#, kde-format
msgid "Running"
msgstr "In esecuzione"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Disinstallabile"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
#, kde-format
msgid "Categories:"
msgstr "Categorie:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:199
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Scarica nuovi oggetti di Plasma"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:208
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Installa oggetto da file locale…"

#~ msgid "&Execute"
#~ msgstr "&Esegui"

#~ msgctxt "Toolbar Button to switch to Plasma Scripting Mode"
#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgctxt "Toolbar Button to switch to KWin Scripting Mode"
#~ msgid "KWin"
#~ msgstr "KWin"

#~ msgid "Templates"
#~ msgstr "Modelli"

#~ msgid "Desktop Shell Scripting Console"
#~ msgstr "Console di creazione script della shell per il desktop"

#~ msgid "Editor"
#~ msgstr "Editor"

#~ msgid "Load"
#~ msgstr "Carica"

#~ msgid "Use"
#~ msgstr "Usa"

#~ msgid "Output"
#~ msgstr "Output"

#~ msgid "Unable to load script file <b>%1</b>"
#~ msgstr "Impossibile caricare il file di script <b>%1</b>"

#~ msgid "Open Script File"
#~ msgstr "Apri file di script"

#~ msgid "Save Script File"
#~ msgstr "Salva file di script"

#~ msgid "Executing script at %1"
#~ msgstr "Esecuzione script alle %1"

#~ msgid "Runtime: %1ms"
#~ msgstr "Tempo di esecuzione: %1ms"

#~ msgid "Download Wallpaper Plugins"
#~ msgstr "Estensioni per lo scaricamento di sfondi"
