# Translation of plasma_applet_org.kde.plasma.mediacontroller.po to Brazilian Portuguese
# Copyright (C) 2014-2016 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2014, 2016.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2016, 2017, 2018, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.mediacontroller\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-09 00:50+0000\n"
"PO-Revision-Date: 2022-06-22 16:26-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: contents/config/config.qml:12
#, kde-format
msgid "General"
msgstr "Geral"

#: contents/ui/AlbumArtStackView.qml:161
#, kde-format
msgid "No title"
msgstr ""

#: contents/ui/AlbumArtStackView.qml:161 contents/ui/main.qml:80
#, kde-format
msgid "No media playing"
msgstr "Nenhuma mídia em reprodução"

#: contents/ui/ConfigGeneral.qml:22
#, kde-format
msgid "Volume step:"
msgstr "Passo do volume:"

#: contents/ui/ExpandedRepresentation.qml:363
#: contents/ui/ExpandedRepresentation.qml:484
#, kde-format
msgctxt "Remaining time for song e.g -5:42"
msgid "-%1"
msgstr "-%1"

#: contents/ui/ExpandedRepresentation.qml:513
#, kde-format
msgid "Shuffle"
msgstr "Embaralhar"

#: contents/ui/ExpandedRepresentation.qml:539 contents/ui/main.qml:98
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "Faixa anterior"

#: contents/ui/ExpandedRepresentation.qml:561 contents/ui/main.qml:106
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "Pausar"

#: contents/ui/ExpandedRepresentation.qml:561 contents/ui/main.qml:111
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "Reproduzir"

#: contents/ui/ExpandedRepresentation.qml:579 contents/ui/main.qml:117
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "Faixa seguinte"

#: contents/ui/ExpandedRepresentation.qml:602
#, kde-format
msgid "Repeat Track"
msgstr "Repetir faixa"

#: contents/ui/ExpandedRepresentation.qml:602
#, kde-format
msgid "Repeat"
msgstr "Repetir"

#: contents/ui/main.qml:94
#, kde-format
msgctxt "Open player window or bring it to the front if already open"
msgid "Open"
msgstr "Abrir"

#: contents/ui/main.qml:123
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "Parar"

#: contents/ui/main.qml:131
#, kde-format
msgctxt "Quit player"
msgid "Quit"
msgstr "Sair"

#: contents/ui/main.qml:241
#, kde-format
msgid "Choose player automatically"
msgstr "Escolher reprodutor automaticamente"

#: contents/ui/main.qml:278
#, kde-format
msgctxt "by Artist (player name)"
msgid "by %1 (%2)"
msgstr "por %1 (%2)"

#: contents/ui/main.qml:289
#, kde-format
msgctxt "by Artist (paused, player name)"
msgid "by %1 (paused, %2)"
msgstr "por %1 (pausado, %2)"

#: contents/ui/main.qml:289
#, kde-format
msgctxt "Paused (player name)"
msgid "Paused (%1)"
msgstr "Pausado (%1)"

#~ msgctxt "artist – track"
#~ msgid "%1 – %2"
#~ msgstr "%1 – %2"

#~ msgctxt "Artist of the song"
#~ msgid "by %1"
#~ msgstr "por %1"

#~ msgid "Pause playback when screen is locked"
#~ msgstr "Pausar a reprodução quando a tela estiver bloqueada"
