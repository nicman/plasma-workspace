# Translation of plasmashellprivateplugin.po to Brazilian Portuguese
# Copyright (C) 2014-2015 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# André Marcelo Alvarenga <alvarenga@kde.org>, 2014, 2015.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2017, 2018, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasmashellprivateplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-03 00:48+0000\n"
"PO-Revision-Date: 2022-06-22 16:27-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Luiz Fernando Ranghetti"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "elchevive@opensuse.org"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Feriados"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Eventos"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Tarefas"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Outros"

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Bloqueio de tela ativo"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Define se a tela será bloqueada após o tempo indicado."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Tempo de espera do protetor de tela"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Define a quantidade de minutos para bloquear a tela."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Nova sessão"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Filtros"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:42
#, kde-format
msgid "Select Plasmoid File"
msgstr "Selecione o arquivo do plasmoide"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:66
#, kde-format
msgid "Installing the package %1 failed."
msgstr "Erro na instalação do pacote %1."

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:66
#, kde-format
msgid "Installation Failure"
msgstr "Falha na instalação"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:110
#, kde-format
msgid "All Widgets"
msgstr "Todos os widgets"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
#, kde-format
msgid "Running"
msgstr "Em execução"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Pode ser desinstalado"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
#, kde-format
msgid "Categories:"
msgstr "Categorias:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:199
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Baixar novos widgets do Plasma"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:208
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Instalar widget de arquivo local..."

#~ msgid "&Execute"
#~ msgstr "&Executar"

#~ msgctxt "Toolbar Button to switch to Plasma Scripting Mode"
#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgctxt "Toolbar Button to switch to KWin Scripting Mode"
#~ msgid "KWin"
#~ msgstr "KWin"

#~ msgid "Templates"
#~ msgstr "Modelos"

#~ msgid "Desktop Shell Scripting Console"
#~ msgstr "Console de criação de script do Desktop Shell"

#~ msgid "Editor"
#~ msgstr "Editor"

#~ msgid "Load"
#~ msgstr "Carregar"

#~ msgid "Use"
#~ msgstr "Usar"

#~ msgid "Output"
#~ msgstr "Resultado"

#~ msgid "Unable to load script file <b>%1</b>"
#~ msgstr "Não foi possível carregar o arquivo de script <b>%1</b>"

#~ msgid "Open Script File"
#~ msgstr "Abrir arquivo de script"

#~ msgid "Save Script File"
#~ msgstr "Salvar arquivo de script"

#~ msgid "Executing script at %1"
#~ msgstr "Executando o script em %1"

#~ msgid "Runtime: %1ms"
#~ msgstr "Tempo de execução: %1ms"

#~ msgid "Download Wallpaper Plugins"
#~ msgstr "Baixar plugins de papel de parede"
