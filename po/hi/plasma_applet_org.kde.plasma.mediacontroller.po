# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Raghavendra Kamath <raghu@raghukamath.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-09 00:50+0000\n"
"PO-Revision-Date: 2021-07-06 19:02+0530\n"
"Last-Translator: Raghavendra Kamath <raghu@raghukamath.com>\n"
"Language-Team: kde-hindi\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 21.04.2\n"

#: contents/config/config.qml:12
#, kde-format
msgid "General"
msgstr ""

#: contents/ui/AlbumArtStackView.qml:161
#, kde-format
msgid "No title"
msgstr ""

#: contents/ui/AlbumArtStackView.qml:161 contents/ui/main.qml:80
#, kde-format
msgid "No media playing"
msgstr "कोई मीडिया नहीं बजा रहा है"

#: contents/ui/ConfigGeneral.qml:22
#, kde-format
msgid "Volume step:"
msgstr ""

#: contents/ui/ExpandedRepresentation.qml:363
#: contents/ui/ExpandedRepresentation.qml:484
#, kde-format
msgctxt "Remaining time for song e.g -5:42"
msgid "-%1"
msgstr "-%1"

#: contents/ui/ExpandedRepresentation.qml:513
#, kde-format
msgid "Shuffle"
msgstr "फेंटें"

#: contents/ui/ExpandedRepresentation.qml:539 contents/ui/main.qml:98
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "पिछले गीत पर जाएँ"

#: contents/ui/ExpandedRepresentation.qml:561 contents/ui/main.qml:106
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "ठहराएँ"

#: contents/ui/ExpandedRepresentation.qml:561 contents/ui/main.qml:111
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "बजाएँ"

#: contents/ui/ExpandedRepresentation.qml:579 contents/ui/main.qml:117
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "अगले गीत पर जाएँ"

#: contents/ui/ExpandedRepresentation.qml:602
#, kde-format
msgid "Repeat Track"
msgstr "गीत दोहराएँ"

#: contents/ui/ExpandedRepresentation.qml:602
#, kde-format
msgid "Repeat"
msgstr "दोहराएँ"

#: contents/ui/main.qml:94
#, kde-format
msgctxt "Open player window or bring it to the front if already open"
msgid "Open"
msgstr "खोलें"

#: contents/ui/main.qml:123
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "रोकें"

#: contents/ui/main.qml:131
#, kde-format
msgctxt "Quit player"
msgid "Quit"
msgstr "बाहर जाएँ"

#: contents/ui/main.qml:241
#, kde-format
msgid "Choose player automatically"
msgstr "स्वचालित रूप से वादक चुनें"

#: contents/ui/main.qml:278
#, kde-format
msgctxt "by Artist (player name)"
msgid "by %1 (%2)"
msgstr "%1 (%2) के द्वारा"

#: contents/ui/main.qml:289
#, kde-format
msgctxt "by Artist (paused, player name)"
msgid "by %1 (paused, %2)"
msgstr "%1 के द्वारा (ठहरा हुआ, %2)"

#: contents/ui/main.qml:289
#, kde-format
msgctxt "Paused (player name)"
msgid "Paused (%1)"
msgstr "ठहरा हुआ (%1)"
