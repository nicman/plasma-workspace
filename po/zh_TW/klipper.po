# translation of klipper.po to Chinese Traditional
# Copyright (C) 2003, 2006, 2007 Free Software Foundation, Inc.
# Jing-Jong Shyue <shyue@sonoma.com.tw>
# Yuan-Chen Cheng <ycheng@sinica.edu.tw>
#
# Keng-Yu Lin <s9321028@ncnu.edu.tw>, 2003.
# 林耕宇 <s9321028@ncnu.edu.tw>, 2003.
# Tsung-Chien Ho <iitze@hotmail.com>, 2003.
# Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>, 2007, 2008, 2009, 2010.
# Franklin Weng <franklin at goodhorse dot idv dot tw>, 2007.
# Frank Weng (a.k.a. Franklin) <franklin@goodhorse.idv.tw>, 2009.
# Franklin Weng <franklin@mail.everfocus.com.tw>, 2010, 2012.
# Franklin Weng <franklin@goodhorse.idv.tw>, 2011, 2014.
# pan93412 <pan93412@gmail.com>, 2018, 2019.
# Chaoting Liu <brli@chakralinux.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: klipper\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-17 00:48+0000\n"
"PO-Revision-Date: 2021-09-06 00:59+0800\n"
"Last-Translator: Chaoting Liu <brli@chakralinux.org>\n"
"Language-Team: Chinese <kde-i18n-doc@kde.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.1\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: configdialog.cpp:80
#, fuzzy, kde-format
#| msgid "Selection and Clipboard"
msgid "Selection and Clipboard:"
msgstr "選取區與剪貼簿"

#: configdialog.cpp:87
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"When text or an area of the screen is highlighted with the mouse or "
"keyboard, this is the <emphasis>selection</emphasis>. It can be pasted using "
"the middle mouse button.<nl/><nl/>If the selection is explicitly copied "
"using a <interface>Copy</interface> or <interface>Cut</interface> action, it "
"is saved to the <emphasis>clipboard</emphasis>. It can be pasted using a "
"<interface>Paste</interface> action. <nl/><nl/>When turned on this option "
"keeps the selection and the clipboard the same, so that any selection is "
"immediately available to paste by any means. If it is turned off, the "
"selection may still be saved in the clipboard history (subject to the "
"options below), but it can only be pasted using the middle mouse button."
msgstr ""

#: configdialog.cpp:106
#, fuzzy, kde-format
#| msgid "Clipboard history"
msgid "Clipboard history:"
msgstr "剪貼簿歷史紀錄"

#: configdialog.cpp:112
#, fuzzy, kde-format
#| msgid " entry"
#| msgid_plural " entries"
msgctxt "Number of entries"
msgid " entry"
msgid_plural " entries"
msgstr[0] " 個項目"

#: configdialog.cpp:131 configdialog.cpp:169
#, fuzzy, kde-format
#| msgid "Replay action in history"
msgid "Always save in history"
msgstr "重做歷史清單中的項目"

#: configdialog.cpp:135
#, fuzzy, kde-format
#| msgid "Text selection only"
msgid "Text selection:"
msgstr "只有選取的文字"

#: configdialog.cpp:137 configdialog.cpp:175
#, kde-format
msgid "Only when explicitly copied"
msgstr ""

#: configdialog.cpp:142
#, fuzzy, kde-format
#| msgid "Really delete entire clipboard history?"
msgid "Whether text selections are saved in the clipboard history."
msgstr "確定要刪除整個剪貼簿的歷史紀錄嗎？"

#: configdialog.cpp:173
#, fuzzy, kde-format
#| msgid "Ignore selection"
msgid "Non-text selection:"
msgstr "忽略選取"

#: configdialog.cpp:180
#, fuzzy, kde-format
#| msgid "Replay action in history"
msgid "Never save in history"
msgstr "重做歷史清單中的項目"

#: configdialog.cpp:185
#, kde-format
msgid ""
"Whether non-text selections (such as images) are saved in the clipboard "
"history."
msgstr ""

#: configdialog.cpp:250
#, fuzzy, kde-format
#| msgid "Timeout for action popups:"
msgid "Show action popup menu:"
msgstr "動作彈跳選單的逾時時間："

#: configdialog.cpp:260
#, kde-kuit-format
msgctxt "@info"
msgid ""
"When text that matches an action pattern is selected or is chosen from the "
"clipboard history, automatically show the popup menu with applicable "
"actions. If the automatic menu is turned off here, or it is not shown for an "
"excluded window, then it can be shown by using the <shortcut>%1</shortcut> "
"key shortcut."
msgstr ""

#: configdialog.cpp:269
#, kde-format
msgid "Exclude Windows..."
msgstr ""

#: configdialog.cpp:283
#, fuzzy, kde-format
#| msgid " second"
#| msgid_plural " seconds"
msgctxt "Unit of time"
msgid " second"
msgid_plural " seconds"
msgstr[0] " 秒"

#: configdialog.cpp:284
#, kde-format
msgctxt "No timeout"
msgid "None"
msgstr ""

#: configdialog.cpp:293
#, fuzzy, kde-format
#| msgctxt "Actions Config"
#| msgid "Actions"
msgid "Options:"
msgstr "動作"

#: configdialog.cpp:307
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The actions shown in the popup menu can be configured on the "
"<interface>Actions Configuration</interface> page."
msgstr ""

#: configdialog.cpp:335
#, kde-format
msgid "Exclude Windows"
msgstr ""

#: configdialog.cpp:365
#, kde-kuit-format
msgctxt "@info"
msgid ""
"When a <interface>match pattern</interface> matches the clipboard contents, "
"its <interface>commands</interface> appear in the Klipper popup menu and can "
"be executed."
msgstr ""

#: configdialog.cpp:374
#, kde-format
msgctxt "@title:column"
msgid "Match pattern and commands"
msgstr ""

#: configdialog.cpp:374
#, fuzzy, kde-format
#| msgid "Description"
msgctxt "@title:column"
msgid "Description"
msgstr "描述"

#: configdialog.cpp:380
#, kde-format
msgid "Add Action..."
msgstr "新增動作..."

#: configdialog.cpp:384
#, kde-format
msgid "Edit Action..."
msgstr "編輯動作..."

#: configdialog.cpp:389
#, kde-format
msgid "Delete Action"
msgstr "刪除動作"

#: configdialog.cpp:396
#, kde-kuit-format
msgctxt "@info"
msgid ""
"These actions appear in the popup menu which can be configured on the "
"<interface>Action Menu</interface> page."
msgstr ""

#: configdialog.cpp:580
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Delete the selected action <resource>%1</resource><nl/>and all of its "
"commands?"
msgstr ""

#: configdialog.cpp:581
#, fuzzy, kde-format
#| msgid "Delete Action"
msgid "Confirm Delete Action"
msgstr "刪除動作"

#: configdialog.cpp:610
#, kde-format
msgctxt "General Config"
msgid "General"
msgstr "一般"

#: configdialog.cpp:610
#, kde-format
msgid "General Configuration"
msgstr "一般設定"

#: configdialog.cpp:611
#, fuzzy, kde-format
#| msgctxt "Actions Config"
#| msgid "Actions"
msgctxt "Popup Menu Config"
msgid "Action Menu"
msgstr "動作"

#: configdialog.cpp:611
#, fuzzy, kde-format
#| msgctxt "Actions Config"
#| msgid "Actions"
msgid "Action Menu"
msgstr "動作"

#: configdialog.cpp:612
#, fuzzy, kde-format
#| msgid "Actions Configuration"
msgctxt "Actions Config"
msgid "Actions Configuration"
msgstr "動作設定"

#: configdialog.cpp:612
#, kde-format
msgid "Actions Configuration"
msgstr "動作設定"

#: configdialog.cpp:615
#, kde-format
msgctxt "Shortcuts Config"
msgid "Shortcuts"
msgstr "捷徑"

#: configdialog.cpp:615
#, kde-format
msgid "Shortcuts Configuration"
msgstr "捷徑設定"

#: configdialog.cpp:693
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The action popup will not be shown automatically for these windows, even if "
"it is enabled. This is because, for example, a web browser may highlight a "
"URL in the address bar while typing, so the menu would show for every "
"keystroke.<nl/><nl/>If the action menu appears unexpectedly when using a "
"particular application, then add it to this list. <link>How to find the name "
"to enter</link>."
msgstr ""

#: configdialog.cpp:706
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"The name that needs to be entered here is the WM_CLASS name of the window to "
"be excluded. To find the WM_CLASS name for a window, in another terminal "
"window enter the command:<nl/><nl/>&nbsp;&nbsp;<icode>xprop | grep WM_CLASS</"
"icode><nl/><nl/>and click on the window that you want to exclude. The first "
"name that it displays after the equal sign is the one that you need to enter."
msgstr ""

#: editactiondialog.cpp:34 editcommanddialog.cpp:90
#, kde-format
msgid "Ignore"
msgstr "忽略"

#: editactiondialog.cpp:36
#, kde-format
msgid "Replace Clipboard"
msgstr "取代剪貼簿"

#: editactiondialog.cpp:38
#, kde-format
msgid "Add to Clipboard"
msgstr "新增到剪貼簿"

#: editactiondialog.cpp:122
#, kde-format
msgid "Command"
msgstr "指令"

#: editactiondialog.cpp:124
#, kde-format
msgid "Output"
msgstr ""

#: editactiondialog.cpp:126
#, kde-format
msgid "Description"
msgstr "描述"

#: editactiondialog.cpp:179
#, kde-format
msgid "Action Properties"
msgstr "動作屬性"

#: editactiondialog.cpp:191
#, kde-kuit-format
msgctxt "@info"
msgid ""
"An action takes effect when its <interface>match pattern</interface> matches "
"the clipboard contents. When this happens, the action's <interface>commands</"
"interface> appear in the Klipper popup menu; if one of them is chosen, the "
"command is executed."
msgstr ""

#: editactiondialog.cpp:203
#, kde-format
msgid "Enter a pattern to match against the clipboard"
msgstr ""

#: editactiondialog.cpp:205
#, kde-format
msgid "Match pattern:"
msgstr ""

#: editactiondialog.cpp:208
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The match pattern is a regular expression. For more information see the "
"<link url=\"https://en.wikipedia.org/wiki/Regular_expression\">Wikipedia "
"entry</link> for this topic."
msgstr ""

#: editactiondialog.cpp:219
#, kde-format
msgid "Enter a description for the action"
msgstr ""

#: editactiondialog.cpp:220 editcommanddialog.cpp:84
#, kde-format
msgid "Description:"
msgstr "描述："

#: editactiondialog.cpp:223
#, kde-format
msgid "Include in automatic popup"
msgstr ""

#: editactiondialog.cpp:227
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The commands for this match will be included in the automatic action popup, "
"if it is enabled in the <interface>Action Menu</interface> page. If this "
"option is turned off, the commands for this match will not be included in "
"the automatic popup but they will be included if the popup is activated "
"manually with the <shortcut>%1</shortcut> key shortcut."
msgstr ""

#: editactiondialog.cpp:262
#, fuzzy, kde-format
#| msgid "Add Command"
msgid "Add Command..."
msgstr "新增命令"

#: editactiondialog.cpp:267
#, fuzzy, kde-format
#| msgid "Add Command"
msgid "Edit Command..."
msgstr "新增命令"

#: editactiondialog.cpp:273
#, fuzzy, kde-format
#| msgid "Remove Command"
msgid "Delete Command"
msgstr "移除命令"

#: editactiondialog.cpp:389
#, kde-kuit-format
msgctxt "@info"
msgid "Delete the selected command <resource>%1</resource>?"
msgstr ""

#: editactiondialog.cpp:390
#, kde-format
msgid "Confirm Delete Command"
msgstr ""

#: editcommanddialog.cpp:47
#, fuzzy, kde-format
#| msgid "Action Properties"
msgid "Command Properties"
msgstr "動作屬性"

#: editcommanddialog.cpp:60
#, kde-format
msgid "Enter the command and arguments"
msgstr ""

#: editcommanddialog.cpp:63
#, fuzzy, kde-format
#| msgid "Command"
msgid "Command:"
msgstr "指令"

#: editcommanddialog.cpp:72
#, kde-kuit-format
msgctxt "@info"
msgid ""
"A <placeholder>&#37;s</placeholder> in the command will be replaced by the "
"complete clipboard contents. <placeholder>&#37;0</placeholder> through "
"<placeholder>&#37;9</placeholder> will be replaced by the corresponding "
"captured texts from the match pattern."
msgstr ""

#: editcommanddialog.cpp:82
#, kde-format
msgid "Enter a description for the command"
msgstr ""

#: editcommanddialog.cpp:92
#, kde-format
msgid "Output from command:"
msgstr ""

#: editcommanddialog.cpp:94
#, fuzzy, kde-format
#| msgid "Replace Clipboard"
msgid "Replace current clipboard"
msgstr "取代剪貼簿"

#: editcommanddialog.cpp:98
#, fuzzy, kde-format
#| msgid "Add to Clipboard"
msgid "Append to clipboard"
msgstr "新增到剪貼簿"

#: editcommanddialog.cpp:102
#, kde-format
msgid "What happens to the standard output of the command executed."
msgstr ""

#: editcommanddialog.cpp:116
#, kde-format
msgid "Reset the icon to the default for the command"
msgstr ""

#: editcommanddialog.cpp:122
#, kde-format
msgid "Icon:"
msgstr ""

#: historyimageitem.cpp:39
#, kde-format
msgid "%1x%2 %3bpp"
msgstr "%1x%2 %3bpp"

#: klipper.cpp:153
#, fuzzy, kde-format
#| msgid "Timeout for action popups:"
msgctxt "@action:inmenu Toggle automatic action"
msgid "Automatic Action Popup Menu"
msgstr "動作彈跳選單的逾時時間："

#: klipper.cpp:176
#, fuzzy, kde-format
#| msgid "C&lear Clipboard History"
msgctxt "@action:inmenu"
msgid "C&lear Clipboard History"
msgstr "清除剪貼簿歷史紀錄(&L)"

#: klipper.cpp:183
#, fuzzy, kde-format
#| msgid "&Configure Klipper…"
msgctxt "@action:inmenu"
msgid "&Configure Klipper…"
msgstr "剪貼簿設定(&C)"

#: klipper.cpp:189
#, fuzzy, kde-format
#| msgctxt "@item:inmenu Quit Klipper"
#| msgid "&Quit"
msgctxt "@action:inmenu Quit Klipper"
msgid "&Quit"
msgstr "離開(&Q)"

#: klipper.cpp:194
#, fuzzy, kde-format
#| msgid "Manually Invoke Action on Current Clipboard"
msgctxt "@action:inmenu"
msgid "Manually Invoke Action on Current Clipboard"
msgstr "手動啟動在現在剪貼簿裡的行為"

#: klipper.cpp:202
#, fuzzy, kde-format
#| msgid "&Edit Contents…"
msgctxt "@action:inmenu"
msgid "&Edit Contents…"
msgstr "編輯內容(%E)…"

#: klipper.cpp:210
#, fuzzy, kde-format
#| msgid "&Show Barcode…"
msgctxt "@action:inmenu"
msgid "&Show Barcode…"
msgstr "顯示條碼(&S)…"

#: klipper.cpp:218
#, fuzzy, kde-format
#| msgid "Next History Item"
msgctxt "@action:inmenu"
msgid "Next History Item"
msgstr "下一個歷史紀錄項目"

#: klipper.cpp:222
#, fuzzy, kde-format
#| msgid "Previous History Item"
msgctxt "@action:inmenu"
msgid "Previous History Item"
msgstr "前一個歷史紀錄項目"

#: klipper.cpp:228
#, fuzzy, kde-format
#| msgid "Open Klipper at Mouse Position"
msgctxt "@action:inmenu"
msgid "Open Klipper at Mouse Position"
msgstr "在滑鼠游標位置開啟 Klipper"

#: klipper.cpp:540
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can enable URL actions later in the <interface>Actions</interface> page "
"of the Clipboard applet's configuration window"
msgstr ""

#: klipper.cpp:579
#, kde-format
msgid "Should Klipper start automatically when you login?"
msgstr "當您登入系統時 Klipper 要自動啟動嗎？"

#: klipper.cpp:580
#, kde-format
msgid "Automatically Start Klipper?"
msgstr "要自動啟動 Klipper 嗎 ?"

#: klipper.cpp:581
#, kde-format
msgid "Start"
msgstr "開始"

#: klipper.cpp:582
#, kde-format
msgid "Do Not Start"
msgstr "不要開始"

#: klipper.cpp:929
#, kde-format
msgid "Edit Contents"
msgstr "編輯內容"

#: klipper.cpp:996
#, kde-format
msgid "Mobile Barcode"
msgstr "行動條碼"

#: klipper.cpp:1043
#, fuzzy, kde-format
#| msgid "Really delete entire clipboard history?"
msgid "Do you really want to clear and delete the entire clipboard history?"
msgstr "確定要刪除整個剪貼簿的歷史紀錄嗎？"

#: klipper.cpp:1044
#, fuzzy, kde-format
#| msgid "C&lear Clipboard History"
msgid "Clear Clipboard History"
msgstr "清除剪貼簿歷史紀錄(&L)"

#: klipper.cpp:1060 klipper.cpp:1069
#, kde-format
msgid "Clipboard history"
msgstr "剪貼簿歷史紀錄"

#: klipper.cpp:1086
#, kde-format
msgid "up"
msgstr "上"

#: klipper.cpp:1093
#, kde-format
msgid "current"
msgstr "目前"

#: klipper.cpp:1100
#, kde-format
msgid "down"
msgstr "下"

#. i18n: ectx: label, entry (Version), group (General)
#: klipper.kcfg:10
#, kde-format
msgid "Klipper version"
msgstr "Klipper 版本"

#. i18n: ectx: label, entry (KeepClipboardContents), group (General)
#: klipper.kcfg:13
#, kde-format
msgid "Save history across desktop sessions"
msgstr ""

#. i18n: ectx: tooltip, entry (KeepClipboardContents), group (General)
#: klipper.kcfg:15
#, kde-format
msgid ""
"Retain the clipboard history, so it will be available the next time you log "
"in."
msgstr ""

#. i18n: ectx: label, entry (PreventEmptyClipboard), group (General)
#: klipper.kcfg:18
#, fuzzy, kde-format
#| msgid "Prevent empty clipboard"
msgid "Prevent the clipboard from being cleared"
msgstr "避免清空剪貼簿"

#. i18n: ectx: whatsthis, entry (PreventEmptyClipboard), group (General)
#: klipper.kcfg:20
#, kde-format
msgid ""
"Do not allow the clipboard to be cleared, for example when an application "
"exits."
msgstr ""

#. i18n: ectx: label, entry (SyncClipboards), group (General)
#: klipper.kcfg:27
#, fuzzy, kde-format
#| msgid "Selection and Clipboard"
msgid "Keep the selection and clipboard the same"
msgstr "選取區與剪貼簿"

#. i18n: ectx: whatsthis, entry (SyncClipboards), group (General)
#: klipper.kcfg:29
#, kde-format
msgid ""
"Content selected with the cursor is automatically copied to the clipboard so "
"that it can be pasted with either a Paste action or a middle-click.<br/><a "
"href=\"1\">More about the selection and clipboard</a>."
msgstr ""

#. i18n: ectx: label, entry (IgnoreSelection), group (General)
#: klipper.kcfg:32
#, fuzzy, kde-format
#| msgid "Ignore selection"
msgid "Ignore the selection"
msgstr "忽略選取"

#. i18n: ectx: whatsthis, entry (IgnoreSelection), group (General)
#: klipper.kcfg:34
#, kde-format
msgid ""
"Content selected with the cursor but not explicitly copied to the clipboard "
"is not automatically stored in the clipboard history, and can only be pasted "
"using a middle-click."
msgstr ""

#. i18n: ectx: label, entry (SelectionTextOnly), group (General)
#: klipper.kcfg:37
#, kde-format
msgid "Text selection only"
msgstr "只有選取的文字"

#. i18n: ectx: whatsthis, entry (SelectionTextOnly), group (General)
#: klipper.kcfg:39
#, kde-format
msgid ""
"Only store text selections in the clipboard history, not images or any other "
"type of data."
msgstr ""

#. i18n: ectx: label, entry (IgnoreImages), group (General)
#: klipper.kcfg:42
#, fuzzy, kde-format
#| msgid "Ignore images"
msgid "Always ignore images"
msgstr "忽略影像"

#. i18n: ectx: whatsthis, entry (IgnoreImages), group (General)
#: klipper.kcfg:44
#, kde-format
msgid ""
"Do not store images in the clipboard history, even if explicitly copied."
msgstr ""

#. i18n: ectx: label, entry (UseGUIRegExpEditor), group (General)
#: klipper.kcfg:47
#, kde-format
msgid "Use graphical regexp editor"
msgstr "使用圖形化的正則表達式編輯器"

#. i18n: ectx: label, entry (URLGrabberEnabled), group (General)
#: klipper.kcfg:51
#, fuzzy, kde-format
#| msgid "Ignore selection"
msgid "Immediately on selection"
msgstr "忽略選取"

#. i18n: ectx: tooltip, entry (URLGrabberEnabled), group (General)
#: klipper.kcfg:52
#, kde-format
msgid ""
"Show the popup menu of applicable actions as soon as a selection is made."
msgstr ""

#. i18n: ectx: label, entry (NoActionsForWM_CLASS), group (General)
#: klipper.kcfg:57
#, kde-format
msgid "No actions for WM_CLASS"
msgstr "沒有 WM_CLASS 的動作"

#. i18n: ectx: label, entry (TimeoutForActionPopups), group (General)
#: klipper.kcfg:60
#, fuzzy, kde-format
#| msgid "Timeout for action popups:"
msgid "Automatic action menu time:"
msgstr "動作彈跳選單的逾時時間："

#. i18n: ectx: tooltip, entry (TimeoutForActionPopups), group (General)
#: klipper.kcfg:64
#, fuzzy, kde-format
#| msgid "Timeout for action popups:"
msgid "Display the automatic action popup menu for this time."
msgstr "動作彈跳選單的逾時時間："

#. i18n: ectx: label, entry (MaxClipItems), group (General)
#: klipper.kcfg:67
#, fuzzy, kde-format
#| msgid "Clipboard history size:"
msgid "History size:"
msgstr "剪貼簿歷史紀錄大小："

#. i18n: ectx: tooltip, entry (MaxClipItems), group (General)
#: klipper.kcfg:71
#, kde-format
msgid "The clipboard history will store up to this many items."
msgstr ""

#. i18n: ectx: label, entry (ActionList), group (General)
#: klipper.kcfg:74
#, kde-format
msgid "Dummy entry for indicating changes in an action's tree widget"
msgstr "在動作樹狀元件裡用於指示變更的空項目"

#. i18n: ectx: label, entry (StripWhiteSpace), group (Actions)
#: klipper.kcfg:87
#, fuzzy, kde-format
#| msgid "Strip whitespace when executing an action"
msgid "Trim whitespace from selection"
msgstr "當執行動作時移除空白字元"

#. i18n: ectx: whatsthis, entry (StripWhiteSpace), group (Actions)
#: klipper.kcfg:89
#, kde-format
msgid ""
"Remove any whitespace from the start and end of selected text, before "
"performing an action. For example, this ensures that a URL pasted in a "
"browser is interpreted as expected. The text saved on the clipboard is not "
"affected."
msgstr ""

#. i18n: ectx: label, entry (ReplayActionInHistory), group (Actions)
#: klipper.kcfg:92
#, fuzzy, kde-format
#| msgid "Replay actions on an item selected from history"
msgid "For an item chosen from history"
msgstr "將從歷史清單中所選的項目重新作用一次"

#. i18n: ectx: tooltip, entry (ReplayActionInHistory), group (Actions)
#: klipper.kcfg:94
#, fuzzy, kde-format
#| msgid "Really delete entire clipboard history?"
msgid ""
"Show the popup menu of applicable actions if an entry is chosen from the "
"clipboard history."
msgstr "確定要刪除整個剪貼簿的歷史紀錄嗎？"

#. i18n: ectx: label, entry (EnableMagicMimeActions), group (Actions)
#: klipper.kcfg:97
#, fuzzy, kde-format
#| msgid "Enable MIME-based actions"
msgid "Include MIME actions"
msgstr "開啟 MIME-based 動作"

#. i18n: ectx: whatsthis, entry (EnableMagicMimeActions), group (Actions)
#: klipper.kcfg:99
#, kde-format
msgid ""
"If a file name or URL is selected, include applications that can accept its "
"MIME type in the popup menu."
msgstr ""

#: klipperpopup.cpp:110
#, kde-format
msgid "Klipper - Clipboard Tool"
msgstr "Klipper - 剪貼簿工具"

#: klipperpopup.cpp:114
#, kde-format
msgid "Search…"
msgstr "搜尋…"

#: klipperpopup.cpp:123 main.cpp:29
#, kde-format
msgid "KDE cut & paste history utility"
msgstr "KDE 剪貼歷程的工具程式"

#: klipperpopup.cpp:187
#, fuzzy, kde-format
#| msgid "Regular expression:"
msgid "Invalid regular expression, %1"
msgstr "正則表達式："

#: klipperpopup.cpp:193 tray.cpp:25 tray.cpp:53
#, kde-format
msgid "Clipboard is empty"
msgstr "剪貼簿是空的"

#: klipperpopup.cpp:195
#, kde-format
msgid "No matches"
msgstr "無相符"

#: main.cpp:27 tray.cpp:22
#, kde-format
msgid "Klipper"
msgstr "Klipper"

#: main.cpp:31
#, kde-format
msgid ""
"(c) 1998, Andrew Stanley-Jones\n"
"1998-2002, Carsten Pfeiffer\n"
"2001, Patrick Dubroy"
msgstr ""
"(c) 1998, Andrew Stanley-Jones\n"
"1998-2002, Carsten Pfeiffer\n"
"2001, Patrick Dubroy"

#: main.cpp:34
#, kde-format
msgid "Carsten Pfeiffer"
msgstr "Carsten Pfeiffer"

#: main.cpp:34
#, kde-format
msgid "Author"
msgstr "作者"

#: main.cpp:36
#, kde-format
msgid "Andrew Stanley-Jones"
msgstr "Andrew Stanley-Jones"

#: main.cpp:36
#, kde-format
msgid "Original Author"
msgstr "原始作者"

#: main.cpp:38
#, kde-format
msgid "Patrick Dubroy"
msgstr "Patrick Dubroy"

#: main.cpp:38
#, kde-format
msgid "Contributor"
msgstr "貢獻者"

#: main.cpp:40
#, kde-format
msgid "Luboš Luňák"
msgstr "Luboš Luňák"

#: main.cpp:40
#, kde-format
msgid "Bugfixes and optimizations"
msgstr "程式錯誤修補和最佳化"

#: main.cpp:42
#, kde-format
msgid "Esben Mose Hansen"
msgstr "Esben Mose Hansen"

#: main.cpp:42
#, kde-format
msgid "Previous Maintainer"
msgstr "前任維護者"

#: main.cpp:44
#, kde-format
msgid "Martin Gräßlin"
msgstr "Martin Gräßlin"

#: main.cpp:44
#, kde-format
msgid "Maintainer"
msgstr "維護者"

#: main.cpp:46
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "鄭原真,林耕宇,Tsung-Chien Ho"

#: main.cpp:46
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ycheng@sinica.edu.tw,s9321028@ncnu.edu.tw,iitze@hotmail.com"

#: popupproxy.cpp:145
#, kde-format
msgid "&More"
msgstr "更多(&M)"

#: tray.cpp:25
#, kde-format
msgid "Clipboard Contents"
msgstr "剪貼簿內容"

#: urlgrabber.cpp:198
#, kde-format
msgid "Disable This Popup"
msgstr "取消這個彈出式視窗"

#: urlgrabber.cpp:204
#, kde-format
msgid "&Cancel"
msgstr "取消(&C)"

#~ msgid "Delete clipboard history?"
#~ msgstr "確定要刪除剪貼簿歷史紀錄？"

#~ msgid "Action list:"
#~ msgstr "動作清單："

#~ msgid "Regular Expression"
#~ msgstr "正則表達式"

#~ msgid ""
#~ "Click on a highlighted item's column to change it. \"%s\" in a command "
#~ "will be replaced with the clipboard contents.<br>For more information "
#~ "about regular expressions, you could have a look at the <a href=\"https://"
#~ "en.wikipedia.org/wiki/Regular_expression\">Wikipedia entry about this "
#~ "topic</a>."
#~ msgstr ""
#~ "點擊突顯項目的欄位來改變。指令中的 \"%s\" 會被取代為剪貼簿的內容。<br/>關"
#~ "於正規表示式的詳情，您可以參考<a href=\"https://en.wikipedia.org/wiki/"
#~ "Regular_expression\">維基百科</a>中的說明。"

#~ msgid "Output Handling"
#~ msgstr "輸出處理"

#~ msgid "new command"
#~ msgstr "新命令"

#~ msgid "Command Description"
#~ msgstr "指令描述"

#~ msgid "Action properties:"
#~ msgstr "動作屬性："

#~ msgid "Automatic:"
#~ msgstr "自動："

#~ msgid "List of commands for this action:"
#~ msgstr "此動作的指令清單："

#~ msgid "Double-click an item to edit"
#~ msgstr "雙擊項目以編輯"

#~ msgid "Remove whitespace when executing actions"
#~ msgstr "當執行時移除空白字元"

#~ msgid "Advanced..."
#~ msgstr "進階..."

#~ msgid "Advanced Settings"
#~ msgstr "進階設定"

#~ msgid "D&isable Actions for Windows of Type WM_CLASS"
#~ msgstr "關閉型態為 WM_CLASS 的視窗行為 (&i)"

#~ msgid ""
#~ "<qt>This lets you specify windows in which Klipper should not invoke "
#~ "\"actions\". Use<br /><br /><center><b>xprop | grep WM_CLASS</b></"
#~ "center><br />in a terminal to find out the WM_CLASS of a window. Next, "
#~ "click on the window you want to examine. The first string it outputs "
#~ "after the equal sign is the one you need to enter here.</qt>"
#~ msgstr ""
#~ "<qt>讓您指定在哪些視窗中 Klipper 不應該呼叫「動作」。在終端機中使用<br /"
#~ "><br /><center><b>xprop | grep WM_CLASS</b></center><br /> 來找出一個視窗"
#~ "的 WM_CLASS。<br />接下來，選擇您所想檢驗的視窗。在等號之後出現的第一個字"
#~ "串便是您應該在這裡輸入的字串。</qt>"

#~ msgid "Enable Clipboard Actions"
#~ msgstr "開啟剪貼簿動作"

#~ msgid "URL grabber enabled"
#~ msgstr "網址擷取器已開啟"

#~ msgid "Replay action in history"
#~ msgstr "重做歷史清單中的項目"

#~ msgid "Save clipboard contents on exit"
#~ msgstr "離開時儲存剪貼簿內容"

#~ msgid "Synchronize contents of the clipboard and the selection"
#~ msgstr "同步化剪貼簿和選取物件的內容"

#~ msgid "Keep clipboard contents"
#~ msgstr "保持剪貼簿內容"

#~ msgid ""
#~ "Selecting this option has the effect, that the clipboard can never be "
#~ "emptied. E.g. when an application exits, the clipboard would usually be "
#~ "emptied."
#~ msgstr ""
#~ "啟動這個選項會讓剪貼簿不被清空。因為，當程式結束時，常常會清空 剪貼簿中的"
#~ "資料。"

#~ msgid "Ignore Selection"
#~ msgstr "忽略選取區"

#~ msgid ""
#~ "When an area of the screen is selected with mouse or keyboard, this is "
#~ "called \"the selection\".<br/>If this option is set, the selection is not "
#~ "entered into the clipboard history, though it is still available for "
#~ "pasting using the middle mouse button."
#~ msgstr ""
#~ "當您使用滑鼠或鍵盤選取螢幕上的某個區塊的時候，這個區塊就叫作「選取區」。"
#~ "<br/>選取此選項時，選取區不會放入剪貼簿中，但是您還是可以用滑鼠中鍵將它貼"
#~ "上。"

#~ msgid "Synchronize clipboard and selection"
#~ msgstr "同步剪貼簿和選取內容"

#~ msgid ""
#~ "When an area of the screen is selected with mouse or keyboard, this is "
#~ "called \"the selection\".<br/>If this option is selected, the selection "
#~ "and the clipboard is kept the same, so that anything in the selection is "
#~ "immediately available for pasting elsewhere using any method, including "
#~ "the traditional middle mouse button. Otherwise, the selection is recorded "
#~ "in the clipboard history, but the selection can only be pasted using the "
#~ "middle mouse button. Also see the 'Ignore Selection' option."
#~ msgstr ""
#~ "當您使用滑鼠或鍵盤選取螢幕上的某個區塊的時候，這個區塊就叫作「選取區」。"
#~ "<br/>選取此選項時，剪貼簿與選取區會保持同步，所以選取區的內容會馬上可以用"
#~ "任何方法貼到任何地方，包括用滑鼠中鍵。若未選取，則選取區會紀錄在剪貼簿中，"
#~ "但是只能用滑鼠中鍵貼上。此外請參考「忽略選取區」選項。"

#~ msgid "Selection text only"
#~ msgstr "只有選取的文字"

#~ msgid ""
#~ "When an area of the screen is selected with mouse or keyboard, this is "
#~ "called \"the selection\".<br/>If this option is selected, only text "
#~ "selections are stored in the history, while images and other selections "
#~ "are not."
#~ msgstr ""
#~ "當您使用滑鼠或鍵盤選取螢幕上的某個區塊的時候，這個區塊就叫作「選取區」。"
#~ "<br/>選取此選項時，只有文字會儲存在剪貼簿歷史紀錄中，影像和其它的物件則不"
#~ "會。"

#~ msgid "Timeout for action popups (seconds)"
#~ msgstr "動作彈跳選單的逾時時間（秒）"

#~ msgid "A value of 0 disables the timeout"
#~ msgstr "數值 0 將會關閉逾時時間"

#~ msgid "Clipboard history size"
#~ msgstr "剪貼簿歷史紀錄大小"

#~ msgid ""
#~ "Sometimes, the selected text has some whitespace at the end, which, if "
#~ "loaded as URL in a browser would cause an error. Enabling this option "
#~ "removes any whitespace at the beginning or end of the selected string "
#~ "(the original clipboard contents will not be modified)."
#~ msgstr ""
#~ "有時，在您所選取的文字的最後會有空白字元，如果是一個網址的話， 這樣的空白"
#~ "字元可能會造成瀏覽器出現錯誤。啟動這個選項，可以移除您所選取的文字頭尾的空"
#~ "白字元。（但是剪貼簿中的原始內容不會更改）"

#~ msgid "%1 - Actions For: %2"
#~ msgstr "%1 - 動作於：%2"

#~ msgid "&Edit Contents..."
#~ msgstr "編輯內容 (&E) ..."

#~ msgid "<empty clipboard>"
#~ msgstr "<空的剪貼簿>"

#~ msgid ""
#~ "You can enable URL actions later by left-clicking on the Klipper icon and "
#~ "selecting 'Enable Clipboard Actions'"
#~ msgstr ""
#~ "您之後可以自行啟動 URL 動作。點擊 Klipper 圖示，然後選擇「開啟剪貼簿動作」"
